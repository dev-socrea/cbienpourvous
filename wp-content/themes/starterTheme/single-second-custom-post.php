<?php get_header(); ?>

<div class="container-fluid">
	<div class="row">
		<main role="main" class="col-lg-8 col-md-8 col-xs-8 main-content">
			<!-- section -->
			<section>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <!-- post thumbnail -->
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php if(has_post_thumbnail()){?>
                                <?php the_post_thumbnail(); ?>
                            <?php }else{ ?>
                                <div style="height: 150px; background-size: contain; background-repeat: no-repeat; background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')"></div>
                            <?php } ?>
                        </a>
                        <!-- /post thumbnail -->

						<!-- post title -->
						<h1>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</h1>
						<!-- /post title -->

						<!-- post details -->
						<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
						<span class="author"><?php _e( 'Published by', 'starterTheme' ); ?> <?php the_author_posts_link(); ?></span>
						<span class="comments"><?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'starterTheme' ), __( '1 Comment', 'starterTheme' ), __( '% Comments', 'starterTheme' )); ?></span>
						<!-- /post details -->

						<?php the_content(); // Dynamic Content ?>

                        <?php
                            echo '<a target="_blank" class="social-network fb" href="https://www.facebook.com/sharer/sharer.php?u='.actual_link().'"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
                            echo '<a target="_blank" class="social-network tw" href="https://twitter.com/intent/tweet?url='.actual_link().'&via=Socreativ"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
                            echo '<a target="_blank" class="social-network gp" href="https://plus.google.com/share?url='.actual_link().'"><i class="fa fa-google-plus" aria-hidden="true"></i></a>';
                        ?>

						<?php the_tags( __( 'Tags: ', 'starterTheme' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

						<p><?php _e( 'Categorised in: ', 'starterTheme' ); the_category(', '); // Separated by commas ?></p>

						<p><?php _e( 'This post was written by ', 'starterTheme' ); the_author(); ?></p>

						<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

						<?php comments_template(); ?>

                        <?php previous_post_link(); ?> | < ?php next_post_link(); ?>

					</article>
					<!-- /article -->

					<?php endwhile; ?>

					<?php else: ?>

						<!-- article -->
						<article>

							<h1><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h1>

						</article>
						<!-- /article -->

					<?php endif; ?>

				</section>
                <!-- /section -->
			</main>
		<div class="col-lg-4 col-md-4 col-xs-4 pull-left right-side-bar"><?php get_sidebar(); ?></div>
	</div>
</div>
<?php get_footer(); ?>
