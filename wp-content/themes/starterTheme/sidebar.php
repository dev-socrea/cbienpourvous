<!-- sidebar -->
<aside class="sidebar main-side-bar" role="complementary">

	<div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('default')) ?>
	</div>

</aside>
<!-- /sidebar -->