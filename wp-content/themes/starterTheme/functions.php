<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here
/*------------------------------------*\
  LOGIN PAGE
\*------------------------------------*/
// Fonction qui insere le lien vers le css qui surchargera celui d'origine
function custom_login_css()  {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('template_directory') . '/login-page/style-login.css" />';
}
add_action('login_head', 'custom_login_css');

// Page option ACF

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}


// logo personnalise
function childtheme_custom_login() {

 echo '<div class="logo-wp-admin">
          <img src="' . get_template_directory_uri() .'/assets/img/logo.svg" alt="" />
      </div>';
}

add_action('login_head', 'childtheme_custom_login');


// Fonction qui permet d'ajouter du contenu juste au dessus de la balise
function add_header_login()  {
    echo '<p id="contact"></p>';
}
add_action('login_header','add_header_login');

function wpdocs_excerpt_more( $more ) {
    return '[.....]';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );




// Filtre qui permet de changer l'attribut title du logo
function custom_title_login($message) {
    return get_bloginfo('description'); // On retourne la description du site
}
add_filter('login_headertitle', 'custom_title_login');


// Fonction qui permet d'ajouter du contenu juste au dessus de la balise
// function add_footer_login()  {
//     echo '      ';
// }
// add_action('login_footer','add_footer_login');

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'thumbnail-500', 500, 500, true); // name, width, height, crop
    add_image_size( 'thumbnail-tier', 333, 500, true); // name, width, height, crop
}

function my_image_sizes($sizes) {
    $addsizes = array(
        "square-large" => __( "Large square image")
    );
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

if (!isset($content_width)) {
    $content_width = 900;
}

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('slick-sz', 700, 500, true); // Slick Slider Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('starterTheme', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// File extention upload allowed
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

// Navigation
function main_nav(){
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function enqueue_scripts() {
  wp_enqueue_script( 'jquery' );
}
  add_action( 'enqueue_scripts', 'enqueue_scripts' );


  //jQuery pour Fancy
  if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
  function my_jquery_enqueue() {
     wp_deregister_script('jquery');
     wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") .
          "://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js", false, null);
     wp_enqueue_script('jquery');
  }

function custom_scripts(){
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	  wp_register_script('conditionizr', get_template_directory_uri() . '/assets/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0', false); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/assets/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1', false); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('cookies', get_template_directory_uri() . '/assets/js/cookies.js', array(), false, true); // Cookies Relative
        wp_enqueue_script('cookies'); // Enqueue it!

        wp_register_script('clickHandler', get_template_directory_uri() . '/assets/js/clickHandler.js', array('jquery'), false, true); // Click Handler
        wp_enqueue_script('clickHandler'); // Enqueue it!

        wp_register_script('ajax', get_template_directory_uri() . '/assets/js/ajax.js', array('jquery'), false, true); // Ajax Relative
        wp_enqueue_script('ajax'); // Enqueue it!

        wp_register_script('scripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), false, false); // Custom scripts
        wp_enqueue_script('scripts'); // Enqueue it!


        wp_register_script('slick', get_template_directory_uri() . '/assets/js/lib/slick-1.6.0.min.js', array('jquery'), '1.6.0', true); // slickSlider script
        wp_enqueue_script('slick'); // Enqueue it!

        wp_register_script('fancy_script', (get_bloginfo('stylesheet_directory')."/assets/js/source/jquery.fancybox.pack.js"));
        wp_enqueue_script('fancy_script');

        wp_enqueue_script( 'lazy', get_template_directory_uri() . '/assets/js/lib/jquery.lazyload.min.js', array('jquery'), '1.0.0', true );
      	wp_enqueue_script( 'lazy' );

        wp_localize_script('ajax', 'ajaxUrl', admin_url('admin-ajax.php'));

        wp_register_script( 'gallery', get_template_directory_uri() . '/assets/js/lib/gallery.js', array('jquery'), '1.0.0', true );
      	wp_enqueue_script( 'gallery' );

        wp_register_script( 'mousewheel', get_template_directory_uri() . '/assets/js/lib/jquery.mousewheel.min.js', array('jquery'), '1.0.0', true );
      	wp_enqueue_script( 'mousewheel' );

        wp_register_script( 'timeline', get_template_directory_uri() . '/assets/js/lib/timeline.js', array('jquery'), '1.0.0', true );
      	wp_enqueue_script( 'timeline' );

    }
}

// Load conditional scripts
function conditional_scripts(){
    if (is_page('pagename')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/assets/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
    if(is_page_template('templates/cover-flow.php')) {
        wp_register_script('resizr', get_template_directory_uri() . '/assets/js/resizr.js', array('jquery'), false, false); // Resizer jQuery plugin
        wp_enqueue_script('resizr'); // Enqueue it!
    }
    if(is_page_template('templates/template-contact-full-width.php') || ('templates/template-contact1.php') || ('templates/template-contact2.php') || ('templates/template-contact3.php')) {
        wp_register_script('mapsApi', get_template_directory_uri() . '/assets/js/mapsApi.js'); // mapsApi script
        wp_enqueue_script('mapsApi'); // Enqueue it!
    }
    if (is_singular()) {
        wp_register_style('slick', get_template_directory_uri() . '/assets/css/slick.css', array(), false, 'all');
        wp_enqueue_style('slick'); // Enqueue it!

        wp_register_style('slickTheme', get_template_directory_uri() . '/assets/css/slick-theme.css', array(), false, 'all');
        wp_enqueue_style('slickTheme'); // Enqueue it!
    }
     if (is_page_template('templates/template-galerie.php')) {
        wp_register_script('parallax_gallery', get_template_directory_uri() . '/assets/js/lib/parallax-gallery.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('parallax_gallery'); // Enqueue it!
    }
}

// Defer and Async Scripts
function add_defer_attribute($tag, $handle) {
    $scripts_to_defer = array('ajax');
    foreach($scripts_to_defer as $defer_script) {
        if ($defer_script !== $handle) return $tag;
        return str_replace(' src', ' defer="defer" src', $tag);
    }
    return $tag;
}

function add_async_attribute($tag, $handle) {
    $scripts_to_async = array('clickHandler', 'scripts');
    foreach($scripts_to_async as $async_script) {
        if ($async_script !== $handle) return $tag;
        return str_replace(' src', ' async="async" src', $tag);
    }
    return $tag;
}

// Load styles
function theme_styles(){
    wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), '3.3.6', 'all');
    wp_enqueue_style('bootstrap'); // Enqueue it!

    wp_register_style('fontawesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7.0', 'all');
    wp_enqueue_style('fontawesome'); // Enqueue it!

    wp_register_style('starterStyle', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('starterStyle'); // Enqueue it!

    wp_enqueue_style('fancy', get_template_directory_uri() . '/assets/js/source/jquery.fancybox.css' );
    wp_enqueue_style('fancy'); // Enqueue it!

    wp_enqueue_style('timeline', get_template_directory_uri() . '/assets/css/timeline.css' );
    wp_enqueue_style('timeline'); // Enqueue it!
}

// Register Navigation
require 'includes/nav_menus.php';

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var){
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist){
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes){
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar')) {
    include 'includes/sidebars.php';
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style(){
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

include 'includes/custom_functions.php';

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination(){
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length){ // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
    return 25;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length){
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = ''){
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Changing excerpt more
// function new_excerpt_more($more) {
//   global $post;
//   remove_filter('excerpt_more', 'new_excerpt_more');
//   return '...';
// }
// add_filter('excerpt_more','new_excerpt_more',11);

// Custom View Article link to Post
function view_article($more){
    global $post;
    return '...';
    // return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'starterTheme') . '</a>';
}

// Remove Admin bar
function remove_admin_bar(){
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function style_remove($tag){
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html ){
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function blankgravatar ($avatar_defaults){
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments(){
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth){
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'custom_scripts'); // Add Custom Scripts
add_action('wp_print_scripts', 'conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'theme_styles'); // Add Theme Stylesheet
add_action('init', 'register_menu'); // Add Menu
add_action('init', 'st_post_type'); // Add Custom Post Type
add_action('init', 'sd_post_type'); // Add Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add HTML5 Pagination
add_action('after_theme_setup', 'test');
// Add Ajax' Actions
add_action('wp_ajax_ajax_sort', 'ajax_sort');
add_action('wp_ajax_nopriv_ajax_sort', 'ajax_sort');

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter('upload_mimes', 'cc_mime_types'); // Allow SVG uploads
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

include_once 'includes/custom_post_types.php';

/*------------------------------------*\
	Ajax Functions
\*------------------------------------*/

include_once 'includes/ajax.php';

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null){
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null){ // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
    return '<h2>' . $content . '</h2>';
}

// script to update local version of google analytics script

// Remote file to download

function my_check_function() {
    if(is_front_page()){
        $remoteFile = 'http://www.google-analytics.com/ga.js';
        $localfile = 'wp-content/themes/starterTheme/assets/js/analytics.js';

        // Connection time out
        $connTimeout = 10;
        $url = parse_url($remoteFile);
        $host = $url['host'];
        $path = isset($url['path']) ? $url['path'] : '/';

        if (isset($url['query'])) {
          $path .= '?' . $url['query'];
        }

        $port = isset($url['port']) ? $url['port'] : '80';
        $fp = @fsockopen($host, '80', $errno, $errstr, $connTimeout );
        if(!$fp){
          // On connection failure return the cached file (if it exist)
          if(file_exists($localfile)){
            readfile($localfile);
          }
        } else {
          // Send the header information
          $header = "GET $path HTTP/1.0\r\n";
          $header .= "Host: $host\r\n";
          $header .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6\r\n";
          $header .= "Accept: */*\r\n";
          $header .= "Accept-Language: en-us,en;q=0.5\r\n";
          $header .= "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n";
          $header .= "Keep-Alive: 300\r\n";
          $header .= "Connection: keep-alive\r\n";
          $header .= "Referer: http://$host\r\n\r\n";
          fputs($fp, $header);
          $response = '';

          // Get the response from the remote server
          while($line = fread($fp, 4096)){
            $response .= $line;
          }

          // Close the connection
          fclose( $fp );

          // Remove the headers
          $pos = strpos($response, "\r\n\r\n");
          $response = substr($response, $pos + 4);

          // Save the response to the local file
          if(!file_exists($localfile)){
            // Try to create the file, if doesn't exist
            fopen($localfile, 'w');
          }

          if(is_writable($localfile)) {
            if($fp = fopen($localfile, 'w')){
              fwrite($fp, $response);
              fclose($fp);
            }
          }
        }
    }
}
add_action( 'wp_head', 'my_check_function' );

?>
