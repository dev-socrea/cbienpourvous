<?php get_header();?>
<?php $args = array(
	'posts_per_page'   => 1,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'date',
	'order'            => 'DESC',
	'include'          => '', 'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post', 'post_mime_type'   => '',
	'post_parent'      => '',
	'author'   => '',
	'post_status'      => 'publish',
	'suppress_filters' => true
);?>
<main class="blog-news">
	<div class="container-fluid fil-dariane scroll-fade-out-fast" style="background: background: -moz-linear-gradient(top, rgba(49,49,49,0.7) 0%, rgba(49,49,49,0.7) 1%, rgba(49,49,49,0.7) 100%);
	background: -webkit-linear-gradient(top, rgba(49,49,49,0.7) 0%,rgba(49,49,49,0.7) 1%,rgba(49,49,49,0.7) 100%);
	background: linear-gradient(to bottom, rgba(49,49,49,0.7) 0%,rgba(49,49,49,0.7) 1%,rgba(49,49,49,0.7) 100%), url(<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>);">
		<div class="row m-b-200">
			<div class="m-t-100 p-b-100 p-t-10 blog-title fast-opacity container">
				<h1 class="fs-40 text-white open p-t-80 p-b-20"><?php _e( 'Blog - <span class="fs-40">Cbienpourvous</span>', 'starterTheme' ); ?></h1>
				<a class="fs-12 open text-white p-r-15 p-l-15" href="<?php echo home_url(); ?>"><i class="fa fa-angle-left m-r-5" aria-hidden="true"></i>Retourner à la page d'accueil</a>
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="container">
			<div class="row m-t-10 row-articles m-t-200-neg">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-5 m-b-10">
				<?php
				if ( have_posts() ) :
					/* Start the Loop */
						while ( have_posts() ) : the_post();
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
								<a href="<?php echo get_permalink($recent["ID"]); ?>">
									<?php
										$count_posts = wp_count_posts();
										$published_posts = $count_posts->publish;
										if ($published_posts >= 3) {
											$taille_grille = 4;
										} else {
											$taille_grille = 12;
										}
									?>
									<div class="p-l-10 p-r-10 p-b-10 p-t-10 col-lg-<?php echo $taille_grille ?> col-md-<?php echo $taille_grille ?> col-xs-12">
										<article class="flex align-end anim-300 recent-post-nav no-padding mosaique" style="height:400px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url('<?php echo $thumb['0'];?>');">
											<div class="titre-content w100 flex p-l-15 p-t-15">
												<div class=" flex p-0">
													<div class="">
														<div class="text-gold fs-18 titre-mosaique">
															<?php the_title('') ?>
														</div>
														<div class="cat-mosaique open fs-14">
															<?php echo get_the_category( $recent["ID"] )[0]->name ?>
														</div>
													</div>
												</div>
												<div class="p-0 flex m-l-auto m-r-15 vertical-align">
													<span class="readmore-none anim-300 fs-15">Voir l'article</span><i class="m-t-10 fa fa-angle-right fs-32 text-orange anim-300" aria-hidden="true"></i>
												</div>
											</div>
										</article>
									</div>
								</a>
					<?php endwhile;?>
				<?php endif;
				?>
			</div>
			</div>
			<div class="row m-t-80 m-b-80">
				<!-- pagination -->
				<div class="text-center">
					<div class="blog-pagination text-gold">
						<?php html5wp_pagination(); ?>
					</div>
				</div>
				<!-- /pagination -->
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
