
<?php get_header(); ?>
<?php get_template_part('includes/title'); ?>
    <div class="container-fluid">
        <div class="row">
            <main role="main" class=" main-content">
                <!-- section -->
                <section>
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <div class="row">
                                <div class="segment gradient col-lg-12 col-md-12 col-xs-12">
                                    <!-- post date -->
                                    <span><i class="fa fa-calendar-o"></i> <?php _e('Published at: ', 'starterTheme') ?></span>
                                    <i><?php the_time('j F Y'); ?></i>
                                    <!-- /post date -->
                                    <!-- related categories -->
                                    <div class="pull-right vertical-center related-cat">
                                        <span><i class="fa fa-tag" aria-hidden="true"></i>
                                            <?php
                                            foreach((get_the_category()) as $category) {
                                                if ($category->cat_name != '') {
                                                echo '<a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> ';
                                                }
                                            }?>
                                         </span>
                                    </div>
                                    <!-- /related categories -->
                                </div>
                            </div>

                            <div class="row">
                            <!-- features section  -->
                                <div class="col-md-12 col-sm-12 col-xs-12 vertical-center features-section">
                                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <!-- single gallery -->
                                            <?php if(!empty($slider['gallery'])) : ?>
                                                <div class="single-gallery">
                                                    <?php foreach($slider['gallery'] as $gallery): ?>
                                                        <div style="width:100%; height: 400px; background-image: url('<?=$gallery['photo']['sizes']['large']; ?>'); background-size: cover; background-position: 50% 50%;background-repeat: no-repeat;"></div>
                                                    <?php endforeach; ?>
                                                </div>
                                            <?php endif; ?>
                                            <!-- /gallery -->
                                            <!-- /description -->
                                            <div class="col-md-12 single-desc">
                                                <!-- post content -->
                                                <?php the_content(); ?>
                                                <?php social_media('google'); social_media('twitter'); social_media('facebook');?>
                                                <!-- /post content -->
                                            </div>
                                            <!-- /description -->
                                        </div>
                                        <!-- /col-md-12 -->
                                    </div>
                                    <!-- /col-md-8 col-md-offset-2 -->
                                </div>
                                <!-- /features section -->
                            </div>
                            <!-- /.row -->
                            <div class="clearfix"></div>

                            <?php get_template_part('paginations/pagination', 'nextprev'); ?>

                        </article>
                        <!-- /article -->

                    <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h1><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h1>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>

                </section>
                <!-- /section -->
            </main>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid-->

<?php get_footer(); ?>
