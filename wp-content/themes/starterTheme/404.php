<?php get_header(); ?>

<div class="container-fluid">
	<div class="row">
		<main role="main" class="col-lg-12 col-md-12 col-xs-12 main-content">
			<section>

				<!-- article -->
				<article id="post-404" class="text-center">

					<h1 class="text-center"><?php _e( 'Page not found', 'starterTheme' ); ?></h1>
					<h2 class="text-center">
						<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'starterTheme' ); ?></a>
					</h2>

				</article>
				<!-- /article -->

			</section>
		<!-- /section -->
		</main>
	</div>
</div>

<?php get_footer(); ?>
