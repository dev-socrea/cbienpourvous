<!-- DEFAULT PAGE -->
<?php get_header(); ?>
<main role="main" class="main-content">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<div class="container-fluid">
		<div class="row presta-title">
			<h1><?php the_title() ?></h1>
		</div>
	</div>

		<?php include ($_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/starterTheme/includes/acf/colonnes.php'); ?>
		<?php endwhile; ?>

	<?php else: ?>
		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
		</article>
		<!-- /article -->
	<?php endif; ?>
	</section>
	<!-- /section -->
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>
