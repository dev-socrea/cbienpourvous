<div class="top-page vertical-center anim-300">
	<a href="#top"><i class="fa fa-angle-up"></i></a>
</div>
<!-- footer -->
<footer class="footer" role="contentinfo" slyle="">
    <!-- footer -->
       <div class="container-fluid footer-color p-l-0">
		   <div class="bg-img-footer">
	            <div class="flex">
	                 <div class="text-container4-left-padding footer-left">
						 <div class=" open fs-16">
							 <div class="logo-footer trait-after-orange m-b-30">
								 <img src="/cbienpourvous/wp-content/themes/starterTheme/assets/img/logo-white-cbienpourvous.svg" alt="">
							 </div>
							 <div class="m-b-15">
								 <a class="text-white" href="tel:<?php the_field('telephone', 'option'); ?>"><i class="fa fa-phone m-r-10" aria-hidden="true"></i><?php the_field('telephone', 'option'); ?></a>
							 </div>
							 <div class="trait-after-orange">
								 <a class="text-white" href="mailto:<?php the_field('mail', 'option'); ?>"><i class="fa fa-envelope-o m-r-10" aria-hidden="true"></i><?php the_field('mail', 'option'); ?></a>
							 </div>
							 <div class="social-footer m-t-20 text-white">
								 <a class="text-white hover-facebook" target="_blank" href="<?php the_field('lien_facebook', 'option'); ?>"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
								 <a class="text-white hover-twitter" target="_blank" href="<?php the_field('lien_twitter', 'option'); ?>"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
								 <a class="text-white hover-google" target="_blank" href="<?php the_field('lien_google+', 'option'); ?>"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
							 </div>
						 </div>
	                 </div>
					 <div class="">
						 <div class="text-container8 footer-right p-l-15">
							 <div class=" p-t-30">
								 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									 <div class="fs-19 text-white open trait-gris-footer">
										 Plan du site
									 </div>
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-white p-l-0 fs-12">
										 <div class="small-trait-orange m-t-20">
											 Conciergerie immobilière
										 </div>
										 <div class="small-trait-orange m-t-20">
											 Travailler pour nous
										 </div>
										 <div class="small-trait-orange m-t-20">
											 Location saisonnière
										 </div>
									 </div>
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-white p-l-0 fs-12">
										 <div class="small-trait-orange m-t-20">
											 Proposer un bien
										 </div>
										 <div class="small-trait-orange m-t-20">
											 Actualités / Blog
										 </div>
										 <div class="small-trait-orange m-t-20">
											 Contact & Horaires
										 </div>
									 </div>
								 </div>
								 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-white p-l-40">
									 <div class="open fs-16 m-b-30">
										 <?php the_field('nom_contact', 'option'); ?>
									 </div>
									 <div class="open fs-14">
										 <?php the_field('adresse', 'option'); ?>
									 </div>
									 <a class="open text-center btn-footer m-t-20" href="#">Nous contacter</a>
								 </div>
							 </div>
		                 </div>
					</div>
	            </div>
			</div>
        </div>

    <!-- /footer -->
	<div class="clearfix"></div>
	<div class="container">
	</div>
</footer>
<!-- /footer -->
<div class="clearfix"></div>
</div>
<!-- /wrapper -->

<?php wp_footer(); ?>

<!-- outdatedbrowser -->
<div id="outdated"></div>
<!-- fin outdatedbrowser -->



<!-- <div id="cookie">
<div id="msgCookie">
<p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies et disposez d'une navigation optimale.</p>
</div>
<div id="cookieOk" onclick="cookieOk()">
<button class="anim-300"><?php _e('I agree', 'starterTheme') ?></button>
</div>
</div> -->



<script type="text/javascript">
verifCookie();
</script>

<script type="text/javascript">
// MISE EN PLACE DE FANCYBOX => LIGHTBOX POUR LES GALERIES PHOTOS
jQuery(document).ready(function($) {
	$('.gallery-icon a').has('img').addClass('fancybox');
	$('.gallery-icon a').has('img').attr('rel', 'group');
	$('.gallery-icon a').click(function () {
		var desc = $(this).attr('title');
		$('.gallery-icon- a').has('img').attr('title', desc, 'rel', [gallery] );
	});
	// LIGHTBOX INIT
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
});
</script>

<script type="text/javascript">
// init lazy sur les images
jQuery("img").each(function() {
	jQuery(this).addClass('lazy');
	var url = jQuery(this).attr("src");
	jQuery(this).removeAttr("src");
	jQuery(this).removeAttr("srcset");
	jQuery(this).attr("data-original", url);
});

// ne pas appliquer sur no-lazy
jQuery(".no-lazy img").each(function() {
	console.log(jQuery(this).attr("data-original"));
	var url2 = jQuery(this).attr("data-original");
	jQuery(this).attr("src", url2);
	jQuery(this).removeClass("lazy");
});

jQuery(function(){
	jQuery("img.lazy").lazyload({
		effect : "fadeIn",
		threshold : 500 // PIXELS A PARTIR DESQUELS L'IMAGE SE CHARGE
	});
});
</script>

<!-- THEME 2 POUR ARTICLE -->
<script type="text/javascript">
function theme2article() {
	$( ".news-container>div:first-of-type" ).removeClass( "col-lg-4 col-md-6 col-xs-12" );
	$( ".news-container>div:first-of-type" ).addClass( "col-lg-12 col-md-12 col-xs-12" );
}
</script>
<!--FIN THEME 2 POUR ARTICLE -->

<!-- THEME 3 POUR ARTICLE -->
<script type="text/javascript">
function theme3article() {
	var divs = $(".news-container section > div");
	for(var i = 1; i < divs.length; i+=2) {
		divs.slice(i, i+2).wrapAll("<div class='row'></div>");
	}

	$(".news-container section >div:first-of-type").wrapAll("<div class='row'></div>");

	jQuery(".news-container section >.row:nth-child(2n+0)>div:nth-child(1)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-8 col-md-8 col-xs-8" );
	})

	jQuery(".news-container section >.row:nth-child(2n+0)>div:nth-child(2)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-4 col-md-4 col-xs-4" );
	})

	jQuery(".news-container section >.row:nth-child(2n+1)>div:nth-child(1)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-4 col-md-4 col-xs-4" );
	})

	jQuery(".news-container section >.row:nth-child(2n+1)>div:nth-child(2)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-8 col-md-8 col-xs-8" );
	})

	$( ".news-container section  .row:nth-child(1)>div" ).removeClass( "col-lg-4 col-md-4 col-xs-4" );
	$( ".news-container section  .row:nth-child(1)>div" ).addClass( "col-lg-12 col-md-12 col-xs-12" );
	console.log($( ".news-container section  .row:nth-child(1)>div" ));
}
</script>
<!--FIN THEME 3 POUR ARTICLE -->

<script type="text/javascript">

// THEME A APPLIQUER
var theme_actif = 3;


if (theme_actif == 1) {
} else if (theme_actif == 2) {
	theme2article();
} else if (theme_actif == 3) {
	theme3article();
}
</script>

<script type="text/javascript">
$( document ).ready(function() {
	$("#outdated").click(function() {
    	console.log("toto");
    	$("#outdated").fadeOut(300);
    });
});
</script>

<script>
function showResult(str) {
	if (str.length==0) {
		document.getElementById("livesearch").innerHTML="";
		document.getElementById("livesearch").style.border="0px";
		return;
	}
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {  // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (this.readyState==4 && this.status==200) {
			document.getElementById("livesearch").innerHTML=this.responseText;
			document.getElementById("livesearch").style.border="1px solid #A5ACB2";
		}
	}
	xmlhttp.open("GET","livesearch.php?q="+str,true);
	xmlhttp.send();
}
</script>

<!-- analytics -->
<script>
(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
	(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
	l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
})(window,document,'script','<?php echo get_bloginfo("template_directory"); ?>/assets/js/analytics.js','ga');
ga('create', 'UA-XXXXXXXX-1', 'auto');
ga('send', 'pageview');
</script>

<script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

</body>
</html>
