<?php
// Declare $var(s)
$gallery = get_field('gallery_single');
// Then, create a array of $var(s)
$slider = array(
    'gallery'    => $gallery
);
?>
<?php get_header(); ?>
<?php get_template_part('includes/title'); ?>
    <div class="container-fluid">
        <div class="row">
            <main role="main" class="col-lg-12 col-md-12 col-xs-12 main-content no-padding">
                <!-- section -->
                <section>
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            
                                <div class="segment gradient col-lg-12 col-md-12 col-xs-12">
                                    <!-- post date -->
                                    <span><i class="fa fa-calendar-o"></i> <?php _e('Published at: ', 'starterTheme') ?></span>
                                    <i><?php the_time('j F Y'); ?></i>
                                    <!-- /post date -->
                                    <!-- related categories -->
                                    <div class="pull-right vertical-center related-cat">
                                        <span><i class="fa fa-tag" aria-hidden="true"></i> <?php foreach(get_the_category() as $cat) echo $cat->cat_name . ' '; ?></span>
                                    </div>
                                    <!-- /related categories -->
                                </div>
                            


                            <!-- features section  -->
                                <div class="grid">
                                    <!-- single gallery -->
                                    <?php if(!empty($slider['gallery'])) : ?>
                                        <div class="single-gallery">
                                            <?php foreach($slider['gallery'] as $gallery): ?>
                                                <div style="width:100%; height: 400px; background-image: url('<?=$gallery['photo']['sizes']['large']; ?>'); background-size: cover; background-position: 50% 50%;background-repeat: no-repeat;"></div>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                    <!-- /gallery -->
                                    <!-- /description -->
                                    <div class="col-md-12 single-desc">
                                        <!-- post content -->
                                         <?php include($_SERVER['DOCUMENT_ROOT']."/wp-content/themes/bulles-de-bonheur/includes/devices-detect.php");?>
                                             <?php if ($tablet_browser > 0) {
                                               // do something for TABLET devices
                                                ?> 
                                               <h1><?php the_title(); ?></h1> 
                                                <?php
                                            }
                                            else if ($mobile_browser > 0) {
                                               // do something for MOBILE devices
                                              ?>
                                               <h1><?php the_title(); ?></h1>
                                                <?php
                                            }
                                            else {
                                               // do something for EVRYTHING else
                                            ?>
                                            <?php
                                        }?>      
                                        <?php the_content(); ?>
                                        <?php social_media('google'); social_media('twitter'); social_media('facebook');?>
                                        <!-- /post content -->
                                    </div>
                                    <!-- /description -->
                                </div>
                                <!-- /col-md-12 -->


                            <div class="clearfix"></div>

                            <?php get_template_part('paginations/pagination', 'nextprev'); ?>

                        </article>
                        <!-- /article -->

                    <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h1><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h1>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>

                </section>
                <!-- /section -->
            </main>

        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid-->

<?php get_footer(); ?>