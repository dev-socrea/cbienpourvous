<?php
// Detect and return the last value in current url path
function self_path(){
    $url = pathinfo($_SERVER['REQUEST_URI']);
    $self = $url['basename'];
    return $self;
}

function tmp_name(){
    $tmp_name = basename(get_page_template());
    return $tmp_name;
}

function social_media($network){
    switch($network):
        case 'facebook':
            $class = 'fb';
            echo "<a target='_blank' class='social-network $class' href='https://www.facebook.com/sharer/sharer.php?u=".actual_link()."'><i class='fa fa-$network' aria-hidden='true'></i></a>";
            break;
        case 'twitter':
            $class = 'tw';
            echo "<a target='_blank' class='social-network $class' href='https://twitter.com/intent/tweet?url=".actual_link()."'><i class='fa fa-$network' aria-hidden='true'></i></a>";
            break;
        case 'google':
            $class = 'gp';
            echo "<a target='_blank' class='social-network $class' href='https://plus.google.com/share?url=".actual_link()."'><i class='fa fa-$network-plus' aria-hidden='true'></i></a>";
            break;
    endswitch;
}

/**
 * Only works for an archive page
 */

// Return the entire current url path
function actual_link(){
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return $actual_link;
}
// To return the current post-type identifier
function obj_name(){
    global $wp_query;
    $obj_name = $wp_query->queried_object->name;
    return $obj_name;
}
// To return the current taxonomy slug
function tax_name(){
    $taxonomy_objects = get_object_taxonomies(obj_name());
    $tax = $taxonomy_objects[1];
    return $tax;
}
// To count posts' current total
function current_total(){
    $total = wp_count_posts(obj_name());
    $total = $total->publish;
    return $total;
}

