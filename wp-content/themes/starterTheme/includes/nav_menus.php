<?php
function register_menu(){
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'starterTheme'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'starterTheme'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'starterTheme'), // Extra Navigation if needed (duplicate as many as you need!)
        'social-menu' => __('Social Menu', 'starterTheme'),
        'burger-menu' => __('Burger Menu', 'starterTheme'),
        'footer-menu' => __('Footer Menu', 'starterTheme')
    ));
}
