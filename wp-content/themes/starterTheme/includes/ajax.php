<?php
function ajax_sort(){
    /* Posted values from ajaxSort.data */
    $post_type  = $_POST['post-type'];
    $taxonomy   = $_POST['taxonomy'];
    $term_id    = $_POST['term-id'];
    $term_slug  = $_POST['term-slug'];
    $offset     = $_POST['offset'];
    $tmp_name   = $_POST['tmp-name'];
    $selector   = $_POST['selector'];

    /* $selector == 'load-more' */
    if($selector != 'null'){
        /* Terms list */
        if($term_id != 'null' && $term_slug != 'null'){
            $args = array(
                'post_type' => $post_type,
                'offset' => $offset,
                'tax_query' => array(
                    array(
                        'taxonomy'  => $taxonomy,
                        'terms'     => $term_id,
                        'field'     => $term_slug
                    ),
                )
            );
            /* All */
        }else{
            $args = array(
                'post_type' => $post_type,
                'offset'    => $offset
            );
        }
        /* $selector == 'term-filter' */
    }else{
        /* Terms list */
        if($term_id != 'null' && $term_slug != 'null'){
            $args = array(
                'post_type' => $post_type,
                'posts_per_page' => $offset,
                'tax_query' => array(
                    array(
                        'taxonomy'  => $taxonomy,
                        'terms'     => $term_id,
                        'field'     => $term_slug
                    ),
                )
            );
            /* All */
        }else{
            $args = array(
                'post_type' => $post_type,
                'posts_per_page' => $offset,
                'tax_query' => array(
                    'taxonomy' => $taxonomy
                )
            );
        }
    }

    $query = new WP_Query($args);

    if ($post_type == 'first-custom-post' && $tmp_name == 'cover-flow.php') {
        if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();
                get_template_part('loops/loop', 'cover-flow');
            endwhile;
        endif;
    } elseif ($post_type == 'first-custom-post' && $tmp_name == 'archive-first-custom-post.php'){
        if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();
                get_template_part('loops/loop', 'ajax');
            endwhile;
        endif;
    } else {
        if ($query->have_posts()) :
            while ($query->have_posts()) : $query->the_post();
                get_template_part('loops/loop', 'ajax');
            endwhile;
        endif;
    }

    die();
}