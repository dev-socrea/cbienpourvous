<?php
// Define Sidebar Widget Header Top
register_sidebar(array(
    'name' => __('Header Top', 'starterTheme'),
    'description' => __('Widget Header Top', 'starterTheme'),
    'id' => 'header-top',
    'class' => '',
    'before_widget' => '<div class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Define Sidebar Widget Footer Top
register_sidebar(array(
    'name' => __('Footer Top', 'starterTheme'),
    'description' => __('Widget Footer Top', 'starterTheme'),
    'id' => 'footer-top',
    'class' => '',
    'before_widget' => '<div class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Define Sidebar Widget Footer
register_sidebar(array(
    'name' => __('Footer', 'starterTheme'),
    'description' => __('Widget Footer', 'starterTheme'),
    'id' => 'footer',
    'class' => '',
    'before_widget' => '<div class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Define Sidebar Widget Sub Footer
register_sidebar(array(
    'name' => __('Sub Footer', 'starterTheme'),
    'description' => __('Widget Sub Footer', 'starterTheme'),
    'id' => 'sub-footer',
    'class' => '',
    'before_widget' => '<div class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Define Default Sidebar
register_sidebar(array(
    'name' => __('Default', 'starterTheme'),
    'description' => __('Default Sidebar', 'starterTheme'),
    'id' => 'default',
    'class' => '',
    'before_widget' => '<div class="%2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Define Sidebar Float Right
register_sidebar(array(
    'name' => __('Right Zone', 'starterTheme'),
    'description' => __('Widget Float Zone Right', 'starterTheme'),
    'id' => 'right-widget',
    'before_widget' => '<div class="%1$s %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Define Sidebar Widget Float Left
register_sidebar(array(
    'name' => __('Left Zone', 'starterTheme'),
    'description' => __('Widget Float Zone Left', 'starterTheme'),
    'id' => 'left-widget',
    'before_widget' => '<div class="%1$s %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));