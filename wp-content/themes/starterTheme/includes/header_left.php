<div class="row menu-section logo-left anim-300">
    <div class=" menu-section-child anim-300 p-l-0 p-r-0 flex">
        <div class="menu-left anim-300 container-logo-menu p-l-15">
            <!-- logo -->
            <div class="logo anim-300 no-lazy">
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white-cbienpourvous.svg" alt="Logo" class="no-lazy logo-img anim-300">
                </a>
            </div>
            <!-- /logo -->
        </div>
        <div style="width: 50%;" class="menu-right anim-300 large-menu">
            <div class="vertical-center"><!-- nav -->
                <div id="menu-btn" class="vertical-center p-l-40">
                  <button>
                    <span></span>
                    <span></span>
                    <span></span>
                  </button>
                </div>
                <nav class="menu-principal nav flex main-navigation-zone anim-300 text-white" role="navigation">
                    <span class="f-s-15 m-l-10 text-white">MENU</span>
                    <span class="m-l-15 m-r-15 fs-22 text-white">|</span>
                   <div> <?php main_nav(); ?></div>
                </nav>
            </div>
            <!-- /nav -->
        </div>
    </div>
</div>
