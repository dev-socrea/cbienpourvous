<?php
function st_post_type(){

    register_taxonomy_for_object_type('custom-taxonomy-1', 'first-custom-post');

    register_post_type('first-custom-post', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('First Custom Post', 'starterTheme'), // Rename these to suit
                'singular_name' => __('First Custom Post', 'starterTheme'),
                'add_new' => __('Add New', 'starterTheme'),
                'add_new_item' => __('Add New Custom Post', 'starterTheme'),
                'edit' => __('Edit', 'starterTheme'),
                'edit_item' => __('Edit Custom Post', 'starterTheme'),
                'new_item' => __('New Custom Post', 'starterTheme'),
                'view' => __('See', 'starterTheme'),
                'view_item' => __('See Custom Post', 'starterTheme'),
                'search_items' => __('Search Custom Post', 'starterTheme'),
                'not_found' => __('Custom Post Not Found', 'starterTheme'),
                'not_found_in_trash' => __('Custom Post Not Found In Trash', 'starterTheme')
            ),
            'public' => true,
            'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail'
            ), // Go to Dashboard Custom post for supports
            'can_export' => true, // Allows export in Tools > Export
            'rewrite' => array('with_front' => false),
            'taxonomies' => array(
                'post_tag'
            ) // Add Post Tags support
        ));

    register_taxonomy(
        'custom-taxonomy-1',
        'first-custom-post',
        array(
            'label'  => __('Custom Taxonomy', 'starterTheme'),
            'labels' =>
                array(
                    'name' 			=> __('Custom Taxonomies', 'starterTheme'),
                    'singular_name' => __('Custom Taxonomy', 'starterTheme'),
                    'all_items' 	=> __('All Custom Taxonomy', 'starterTheme'),
                    'edit_item' 	=> __('Edit Custom Taxonomy', 'starterTheme'),
                    'view_item' 	=> __('See Custom Taxonomy', 'starterTheme'),
                    'update_item' 	=> __('Update Custom Taxonomy', 'starterTheme'),
                    'add_new_item' 	=> __('Add Custom Taxonomy', 'starterTheme'),
                    'new_item_name' => __('New Custom Taxonomy', 'starterTheme'),
                    'search_items' 	=> __('Search Custom Taxonomy', 'starterTheme'),
                    'popular_items' => __('Popular Custom Taxonomy', 'starterTheme')
                ),
            'hierarchical' => true,
            'public' => true,
            'has_archive' => true,
        )
    );
}

function sd_post_type(){
    register_taxonomy_for_object_type('post_tag', 'starterTheme');
    register_taxonomy_for_object_type('custom-taxonomy-2', 'second-custom-post');
    register_post_type('second-custom-post', // Register Custom Post Type
        array(
            'labels' => array(
                'name' => __('Second Custom Post', 'starterTheme'), // Rename these to suit
                'singular_name' => __('Second Custom Post', 'starterTheme'),
                'add_new' => __('Add New', 'starterTheme'),
                'add_new_item' => __('Add New Custom Post', 'starterTheme'),
                'edit' => __('Edit', 'starterTheme'),
                'edit_item' => __('Edit Custom Post', 'starterTheme'),
                'new_item' => __('New Custom Post', 'starterTheme'),
                'view' => __('See', 'starterTheme'),
                'view_item' => __('See Custom Post', 'starterTheme'),
                'search_items' => __('Search Custom Post', 'starterTheme'),
                'not_found' => __('Custom Post Not Found', 'starterTheme'),
                'not_found_in_trash' => __('Custom Post Not Found In Trash', 'starterTheme')
            ),
            'public' => true,
            'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail'
            ), // Go to Dashboard Custom post for supports
            'can_export' => true, // Allows export in Tools > Export
            'taxonomies' => array(
                'post_tag'
            ) // Add Post Tags support
        ));

    register_taxonomy(
        'custom-taxonomy-2',
        'second-custom-post',
        array(
            'label'  => __('Custom Taxonomy', 'starterTheme'),
            'labels' =>
                array(
                    'name' 			=> __('Custom Taxonomies', 'starterTheme'),
                    'singular_name' => __('Custom Taxonomy', 'starterTheme'),
                    'all_items' 	=> __('All Custom Taxonomy', 'starterTheme'),
                    'edit_item' 	=> __('Edit Custom Taxonomy', 'starterTheme'),
                    'view_item' 	=> __('See Custom Taxonomy', 'starterTheme'),
                    'update_item' 	=> __('Update Custom Taxonomy', 'starterTheme'),
                    'add_new_item' 	=> __('Add Custom Taxonomy', 'starterTheme'),
                    'new_item_name' => __('New Custom Taxonomy', 'starterTheme'),
                    'search_items' 	=> __('Search Custom Taxonomy', 'starterTheme'),
                    'popular_items' => __('Popular Custom Taxonomy', 'starterTheme')
                ),
            'hierarchical' => true,
            'public' => true,
            'has_archive' => true,
        )
    );
}