<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="https://plus.google.com/XXXXXXXX/" rel="publisher" />
        <link href="https://www.facebook.com/XXXXXXXX/" rel="publisher" />
        <link href="https://www.instagram.com/XXXXXXXX/" rel="publisher" />

        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/manifest.json">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/favicon.ico">
        <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/assets/img/icons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">

		<link title="timeline-styles" rel="stylesheet" href="https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Spectral:400,500,600" rel="stylesheet">


		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php wp_head(); ?>
		<script>
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri().'/assets'; ?>',
            tests: {}
        });
        </script>
	</head>
	<body <?php body_class(); ?>>
        <div id="page-loader"></div>



		<!-- wrapper -->
		<div class="wrapper anim-300">

			<!-- header -->
			<header class="header clearfix anim-300" role="banner">

                 <div id="header-sticky" class="anim-300">
                     <!-- header top -->
                        <!-- <div id="header-top-container" class="anim-300">
                        	<div class="container-fluid anim-300">
    	                        <div class="row anim-300">
    	                             <div class="col-lg-12 col-md-12 col-xs-12 anim-300">
                       					<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Top')) ?>
                       				</div>
                       			</div>
                       		</div>
                        </div> -->
                    <!-- /header top -->



                    <!-- nav -->
                    <!-- /responsive nav -->
                    <?php require 'includes/header_left.php'; ?>
                    <?php //require 'includes/header_center.php'; ?>
                    <div class="container-fluid menu-container anim-300">
                        <!-- responsive nav -->
                        <nav class="nav-mobile" role="navigation">
                            <?php wp_nav_menu( array( 'theme_location' => 'burger-menu' ) ); ?>
                        </nav>
                    </div>
                    <!-- /nav -->

                    <!-- Side-bar fixed -->
                    <div class="fixed-side-area anim-300">
                        <?php get_sidebar('float-left'); ?>
                        <?php get_sidebar('float-right'); ?>
                    </div>
				</div>
				<div id="top" class="sticky-spacing anim-300"></div>
			</header>
			<div class="clearfix"></div>
			<!-- /header -->
