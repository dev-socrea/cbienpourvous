(function($) {

    /**
     * jQuery Resizr Plugin
     * Instanciations : {{get_template_directory_uri}}/assets/js/ajax.js => complete function
     *                : {{get_template_directory_uri}}/archive.php
     **/

    $.fn.resizr = function(options){

        var defaults = {
            currentTotal : -1,
            offsetVisible : 5,
            ratioHover : 2
        };

        var options = $.extend(defaults, options);

        var container = $('section.items-container'),
            parent = container.parent(),
            post = $('li.item'),
            wContainerPercent = 100 + (defaults.currentTotal - defaults.offsetVisible) * (Math.floor((100 / defaults.offsetVisible))),
            wPostPercent = ((100 + (defaults.currentTotal - defaults.offsetVisible) * (Math.floor((100 / defaults.offsetVisible)))) / defaults.currentTotal) * 100;

        var initial = function(){
            if (defaults.currentTotal == defaults.offsetVisible) {
                container.width(parent.width());
                post.width(100 / defaults.offsetVisible + "%");
            } else if (defaults.currentTotal < defaults.offsetVisible) {
                container.width(parent.width());
                post.width(100 / defaults.currentTotal + "%");
            } else {
                // Absolute (px)
                container.width(parent.width() + (defaults.currentTotal - defaults.offsetVisible) * (parent.width() / defaults.offsetVisible));
                // Relative (%)
                /*container.width(wContainerPercent + "%");*/
                post.width((wPostPercent / wContainerPercent) + "%");

            }
        };
        initial();

        $(window).resize(function(){
            initial();
        });

        post.each(function(){
            $(this).hover(
                function(){
                    if (defaults.currentTotal == 1) {
                        container.width(parent.width());
                        post.width(100 + "%");
                    } else if (defaults.currentTotal >= 2) {
                        if(defaults.currentTotal == 2){
                            defaults.ratioHover = 1.25;
                        }
                        // Relative (%)
                        $(this).width((100 / defaults.currentTotal) * defaults.ratioHover + "%");
                        post.not($(this)).width((100 - (100 / defaults.currentTotal) * defaults.ratioHover) / (defaults.currentTotal - 1) + "%");
                    }
                }, function(){
                    initial();
                });
        });

        /**
         * Event Mouse Wheel JavaScript
         * Taken and adapted from : http://www.dte.web.id/2013/02/event-mouse-wheel.html
         **/

        var elem = document.getElementById('scroll-area'),
            childStyle = elem.children[0].style,
            width = parseInt(elem.offsetWidth, 10),
            cldWidth = parseInt(elem.children[0].offsetWidth, 10),
            distance = cldWidth - width,
            speed = 63,
            current = 0;

        childStyle.left = current + "px";

        var scrollH = function (e) {
            e = window.event || e;
            var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
            if ((delta == -1 && current * speed >= -distance) || (delta == 1 && current * speed < 0)) {
                current = current + delta;
                if (childStyle.width > window.innerWidth + 'px') {
                    childStyle.left = (current * speed) + 'px';
                } else {
                    return false;
                }
            }
            e.preventDefault();
        };

        if (elem.addEventListener) {
            // IE9+, Chrome, Safari, Opera
            elem.addEventListener("mousewheel", scrollH, false);
            // Firefox
            elem.addEventListener("DOMMouseScroll", scrollH, false);
        } else {
            // IE 6/7/8
            elem.attachEvent("onmousewheel", scrollH);
        }

    };

})(jQuery, this);