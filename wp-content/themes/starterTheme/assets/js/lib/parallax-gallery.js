/* Effet parallax */
$(document).ready(function() {

var ran1 = (Math.floor((Math.random() * 3) + 1));
var ran2 = (Math.floor((Math.random() * 4) + 2));
var ran3 = (Math.floor((Math.random() * 5) + 3));
var footercol = jQuery(".galerie_container").height();
$(window).scroll(function() {

	// NOMBRE DE PIXELS SCROLL
	var scroll = -($(window).scrollTop());

				$('.galerie_container div:nth-child(2n+1)').css({
					transform: "translate3d(0, "+ scroll/ran1 +"px, 0)" // AJOUT DE PIXEL A LA DIV DE GAUCHE POUR L'EFFET STATIC
				});
				$('.galerie_container div:nth-child(2n)').css({
					transform: "translate3d(0, "+ scroll/ran2 +"px, 0)" // AJOUT DE PIXEL A LA DIV DE GAUCHE POUR L'EFFET STATIC
				});
				$('.galerie_container div:nth-child(3n+1)').css({
					transform: "translate3d(0, "+ scroll/ran3 +"px, 0)" // AJOUT DE PIXEL A LA DIV DE GAUCHE POUR L'EFFET STATIC
				});
	});
});	