(function ($, root, undefined) {

	$(function () {

        $(document).ready(function(){

			// select conciergerie
			var compt_conciergerie = 0;
			jQuery(".click-select-conciergerie").on("click",function(){
				if (compt_conciergerie == 0) {
					$(".select-items-conciergerie").fadeIn(300);
					$(".rotate-click").css({'transform' : 'rotate(180deg)'})
					compt_conciergerie = 1;
				} else {
					$(".select-items-conciergerie").fadeOut(300);
					$(".rotate-click").css({'transform' : 'rotate(0deg)'})
					compt_conciergerie = 0;
				}
			});

			jQuery(".top-page").fadeOut();

            'use strict';
						$("ul,li,a").addClass("anim-300");
                        $( "ul.sub-menu").removeClass("anim-300");



                /* DOM Dimensions */

				$(window).on("load resize", function () {

                    var DOM = {
                        W: $(window).width(),
                        H: $(window).height(),
                        dimensions: function(){
                            console.clear();
                        }
                    };

                    //DOM.dimensions();
                    var header_top_height = $('#header-sticky').outerHeight();
                    $(".sticky-spacing").css('height', header_top_height)
                    ////// SETTING STICKY SCROLL ////////
                    var lastScrollTop = 0;
                    $(window).scroll(function(event){
                        var st = $(this).scrollTop();
                        var header_sticky = 200;
                        if (st > lastScrollTop){
                             if (st >= header_sticky) {
                                $("body").addClass("sticky");
                            }
                        } else {
                            if (st <= header_sticky) {
                                $("body").removeClass("sticky");
                            }
                        }
                       lastScrollTop = st;
                    });


                    /*** PAGE REJOIGNEZ NOUS ****/

                    if ( $('.presta-1:nth-child(2n+1) .presta-B > div').hasClass('text-container')) {
                         $('.presta-1:nth-child(2n+1) .presta-B > div').removeClass('text-container');
                         $('.presta-1:nth-child(2n+1) .presta-B > div').addClass('text-container-left');
                    }
                     /*** PAGE HOME STAGING ****/

                    if ( $('.presta-1:nth-child(2n+1) .presta-D > div').hasClass('text-container')) {
                         $('.presta-1:nth-child(2n+1) .presta-D > div').removeClass('text-container');
                         $('.presta-1:nth-child(2n+1) .presta-D > div').addClass('text-container-left');
                    }

                    if ( $('.travaux-sections:nth-child(2n+1) > div').hasClass('text-container')) {
                         $('.travaux-sections:nth-child(2n+1) > div').removeClass('text-container');
                         $('.travaux-sections:nth-child(2n+1) > div').addClass('text-container-left-p');
                    }

                    /** SINGLE ***/ 

                   var blocTitle =  $('.bloc-single-title').outerHeight();
                    $('.article-recent-single').css('margin-top', blocTitle);


                     /*  CONTAINER FULL WIDTH TEXT GRID */
                     var w_container = $('.container').width();
                     var w_window =$( window ).width();
                     $('.text-container').css("width", w_container/2);
                     $('.text-container-left').css("width", w_container/2);
                     $('.text-container-left').css("margin-left",(w_window - w_container)/2);
                     $('.text-container-left-p').css("padding-left",(w_window - w_container)/2);

                     $('.text-container4').css('width', w_container/3);
                     $('.text-container4-left').css("width", w_container/3);
                     $('.text-container4-left').css("margin-left",(w_window - w_container)/2);

                     $('.text-container4-padding').css('width', w_container/3);
                     $('.text-container4-left-padding').css("width", (w_container/3) + (w_window - w_container)/2);
                     $('.text-container4-left-padding').css("padding-left",(w_window - w_container)/2);

                     $('text-container7').css('width', w_container/2.4);
                     $('.text-container7-left').css("width", w_container/2.4);
                     $('.text-container7-left').css("margin-left",(w_window - w_container)/2);

                     $('.text-container8').css('width', w_container/1.5);
                     $('.text-container8-left').css("width", w_container/1.5);
                     $('.text-container8-left').css("margin-left",(w_window - w_container)/2);



                     var wf_container = $('.container-fluid').width();
                     $('.text-container-fluid').css("width", wf_container/2);


                }).resize();
				$('#page-loader').fadeOut(300);

            });

                /*SUB MENU HEADER LEFT */
                $( ".sub-menu" ).wrap( "<div class='sub-menu-top-position anim-300'></div>" );
                $('.main-navigation-zone > div > ul > li, .nav-mobile > ul > li').on("click",function(){

                    var containerWidth = $(".container.menu-container").outerWidth();
                    $('.sub-menu').css("width", containerWidth);
                    $(this).find(".sub-menu").slideToggle();
                    $(this).siblings().find(".sub-menu").fadeOut(300);

                    if($(".sub-menu:visible").length === 0 ){
                      $("#menu-overlay").fadeOut(300);
                    }else {
                      $("#menu-overlay").fadeIn(300);
                    }
                });
                var Wwidth = $(window).width();
                var containerWidth = $(".container.menu-container").outerWidth();
                $('.sub-menu').css("margin", "auto");
                $("#menu-overlay").on("click",function(){
                    $(".sub-menu").fadeOut(300);
                    $(this).fadeOut(300);
                });

                /* SUB MENU NAV MOBILE */


            /*** PAGE REJOIGNEZ NOUS ****/

            if ( $('.presta-1:nth-child(2n+1) .presta-B > div').hasClass('text-container')) {
                 $('.presta-1:nth-child(2n+1) .presta-B > div').removeClass('text-container');
                 $('.presta-1:nth-child(2n+1) .presta-B > div').addClass('text-container-left');
            }

             /*** PAGE HOME STAGING ****/

            if ( $('.presta-1:nth-child(2n+1) .presta-D > div').hasClass('text-container')) {
                 $('.presta-1:nth-child(2n+1) .presta-D > div').removeClass('text-container');
                 $('.presta-1:nth-child(2n+1) .presta-D > div').addClass('text-container-left');
            }

             /*  CONTAINER FULL WIDTH TEXT GRID */
             var w_container = $('.container').width();
             var w_window =$( window ).width();
             $('.text-container').css("width", w_container/2);
             $('.text-container-left').css("width", w_container/2);
             $('.text-container-left').css("margin-left",(w_window - w_container)/2);
             $('.text-container-left-p').css("padding-left",(w_window - w_container)/2);

             $('.text-container4').css('width', w_container/3);
             $('.text-container4-left').css("width", w_container/3);
             $('.text-container4-left').css("margin-left",(w_window - w_container)/2);

             $('.text-container4-padding').css('width', w_container/3);
             $('.text-container4-left-padding').css("width", (w_container/3) + (w_window - w_container)/2);
             $('.text-container4-left-padding').css("padding-left",(w_window - w_container)/2);

             $('text-container7').css('width', w_container/2.4);
             $('.text-container7-left').css("width", w_container/2.4);
             $('.text-container7-left').css("margin-left",(w_window - w_container)/2);

             $('.text-container8').css('width', w_container/1.5);
             $('.text-container8-left').css("width", w_container/1.5);
             $('.text-container8-left').css("margin-left",(w_window - w_container)/2);



             var wf_container = $('.container-fluid').width();
             $('.text-container-fluid').css("width", wf_container/2);


            /** PAGE HOME ****/




             /********************/
             /*                  */
             /* Page prestation2 */
             /*                  */
             /********************/

             // SLIDER VERTICAL
            //  $('.presta-slider').slick({
            //      vertical: true,
            //      autoPlay:true,
            //      dots: true,
            //      arrows: true,
            //      slidesToShow: 3,
            //      infinite: false,
            //      responsive: [
            //      {
            //         breakpoint: 768,
            //         settings: {
            //         vertical: false,
            //          }
            //         },
            //         {
            //         breakpoint: 500,
            //         settings:{
            //         vertical: false,
            //         slidesToShow: 2
            //         }
            //       },
            //         {
            //         breakpoint: 400,
            //         settings:{
            //         vertical: false,
            //         slidesToShow: 1
            //         }
            //       }
            //      ]
            //  });
             // ANIM AU CLICK INDEXATION DES vignettes
            $(".descriptif > div").fadeOut(300);
            $(".descriptif > div:first-of-type").fadeIn(300);
             $(function() {
                 $('.presta-slider .presta-2').click(
                     function() {
                         $(".descriptif .descriptif-content").fadeOut(300);
                         var idx = $('.presta-slider .presta-2').index(this);
                         $('.descriptif .descriptif-content').eq(idx).delay(300).fadeIn(300);
                 })
             });

			 $(".detailtime > .descriptif-content").fadeOut(300);

			 $(function() {
				//  AFFICHAGE SEMAINE DE BASE
				 $('.septjours > div').unwrap();
				 $('.unmois > div').unwrap();
				 $('.uneannees > div').unwrap();
					 for (var j = 1; j <= 52; j++) {
						 var tmp = $();
						 if (j >= 0 && j <= 9) {
							 var classsemaine = ".S0" + j;
						 } else {
							 var classsemaine = ".S" + j;
						 }
						 $(classsemaine).wrapAll( "<div class='septjours anim-300'></div>" );
					 }
					 var nbrseptjours = $('.septjours').length;
					 var widthseptjours = $('.septjours').first().outerWidth();
					 $('.slidetime2').width(nbrseptjours * widthseptjours);

					$( ".slidetime" ).scrollLeft(nbrseptjours * widthseptjours);





				//  AFFICHAGE CONTENT CLICK ARTICLE
				 $('.presta-4 .eventjourtime ').click(
					 function() {
						 $(".detailtime .descriptif-content").fadeOut(300);
						 var idx = $('.presta-4 .eventjourtime  ').index(this);
						 $('.detailtime .descriptif-content').eq(idx).delay(300).fadeIn(300);
				 })
				 $( ".jourtime" ).each(function() {
					 if ($(this).text().length <= 470) {
					     $(this).remove();
					   }
				   })

				   $( ".jourtime" ).each(function() {
  					 if ($(this).width() == 0) {
  					     $(this).remove();
  					   }
  				   })

				   $( ".dateevent" ).each(function( index ) {
  					 if ($(this).text().length == 36) {
						 $(this).addClass('b-t-date')
  					   }
  				   })
				//    CLICK BOUTON DEZOOM
				 $('.dezoom').click(function(){
					 var ws = $('.septjours').width();
					 $('.septjours').width(ws - 20);
					 var ws = $('.unmois').width();
					 $('.unmois').width(ws - 20);
					 var ws = $('.uneannees').width();
					 $('.uneannees').width(ws - 20);
					 if ($('.septjours').length > 0) {
						 var nbrseptjours = $('.septjours').length;
						 var widthseptjours = $('.septjours').first().outerWidth();
						 $('.slidetime2').width(nbrseptjours * (widthseptjours - 20));
					 }
					 if ($('.unmois').length > 0) {
						 var nbrunmois = $('.unmois').length;
						 var widthunmois = $('.unmois').first().outerWidth();
						 $('.slidetime2').width(nbrunmois * (widthunmois - 20));
					 }
					 if ($('.uneannees').length > 0) {
						 var nbruneannees = $('.uneannees').length;
						 var widthuneannees = $('.uneannees').first().outerWidth();
						 $('.slidetime2').width(nbruneannees * (widthuneannees - 20));
					 }
				 });

				//  CLICK BOUTON ZOOM
				 $('.zoom').click(function(){
					 var ws = $('.septjours').width();
					 $('.septjours').width(ws + 20);
					 var ws = $('.unmois').width();
					 $('.unmois').width(ws + 20);
					 var ws = $('.uneannees').width();
					 $('.uneannees').width(ws + 20);
					 if ($('.septjours').length > 0) {
						 var nbrseptjours = $('.septjours').length;
						 var widthseptjours = $('.septjours').first().outerWidth();
						 $('.slidetime2').width(nbrseptjours * (widthseptjours + 20));
					 }
					 if ($('.unmois').length > 0) {
						 var nbrunmois = $('.unmois').length;
						 var widthunmois = $('.unmois').first().outerWidth();
						 $('.slidetime2').width(nbrunmois * (widthunmois + 20));
					 }
					 if ($('.uneannees').length > 0) {
						 var nbruneannees = $('.uneannees').length;
						 var widthuneannees = $('.uneannees').first().outerWidth();
						 $('.slidetime2').width(nbruneannees * (widthuneannees + 20));
					 }
				 });

				//  CLICK BOUTON SEMAINE
				 $('.semaine').click(function(){
					 $( ".nommois" ).remove();
					 $('.septjours > div').unwrap();
					 $('.unmois > div').unwrap();
					 $('.uneannees > div').unwrap();
						 for (var j = 1; j <= 52; j++) {
							 var tmp = $();
							 if (j >= 0 && j <= 9) {
								 var classsemaine = ".S0" + j;
							 } else {
								 var classsemaine = ".S" + j;
							 }
							 $(classsemaine).wrapAll( "<div class='septjours anim-300'></div>" );
						 }
						 var nbrseptjours = $('.septjours').length;
						 var widthseptjours = $('.septjours').first().outerWidth();
						 $('.slidetime2').width(nbrseptjours * widthseptjours);

					    $( ".slidetime" ).scrollLeft(nbrseptjours * widthseptjours);
						$(this).removeClass('opacity5');
						$('.mois').addClass('opacity5');
						$('.annees').addClass('opacity5');
				 });

				//  CLICK BOUTON MOIS
				 $('.mois').click(function(){
					 $('.septjours > div').unwrap();
					 $('.unmois > div').unwrap();
					 $('.uneannees > div').unwrap();
					 for (var j = 1; j <= 12; j++) {
						 var tmp = $();
						 if (j >= 0 && j <= 9) {
						 	var classmois = ".M0" + j;
						} else {
							var classmois = ".M" + j;
						}
						var nommois = [' ','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
						 $(classmois).wrapAll( "<div class='unmois anim-300'></div>" );
						 var moinsj = j-1;
						 $(".unmois:nth(" + moinsj + ")").append( "<div class='nommois'>" + nommois[j] + "</div>")
					 }

					 var nbrunmois = $('.unmois').length;
					 var widthunmois = $('.unmois').first().outerWidth();
					 $('.slidetime2').width(nbrunmois * widthunmois);

					 $( ".slidetime" ).scrollLeft(nbrunmois * widthunmois);
					 $(this).removeClass('opacity5');
					 $('.semaine').addClass('opacity5');
					 $('.annees').addClass('opacity5');
				 });

				//  CLICK BOUTON ANNEES
				 $('.annees').click(function(){
					 $( ".nommois" ).remove();
					 $('.septjours > div').unwrap();
					 $('.unmois > div').unwrap();
					 $('.uneannees > div').unwrap();
					 for (var j = 2016; j <= 2022; j++) {
						 var tmp = $();
						 if (j >= 0 && j <= 9) {
						 	var classannees = ".A0" + j;
						} else {
							var classannees = ".A" + j;
						}
						$(classannees).wrapAll( "<div class='uneannees anim-300'></div>" );
					 }

					 var nbruneannees = $('.uneannees').length;
					 var widthuneannees = $('.uneannees').first().outerWidth();
					 $('.slidetime2').width(nbruneannees * widthuneannees);
					 $( ".slidetime" ).scrollLeft(nbruneannees * widthuneannees);

					 $(this).removeClass('opacity5');
					 $('.mois').addClass('opacity5');
					 $('.semaine').addClass('opacity5');
				 });

				//  SCROLL HORINZONTALE MOLLETTE SOURIS
				//  $(".slidetime").mousewheel(function(event, delta) {
				//       this.scrollLeft -= (delta * 30);
				//       event.preventDefault();
				//    });
			 });




              /********************/
              /*                  */
              /* Page prestation3 */
              /*                  */
              /********************/

                 $(".p3-content").fadeOut(300);
                 // ROW THUMBNAIL
                 var wrapThumb = $(".presta-3");
                 for(var i = 0; i < wrapThumb.length; i+=4) {
                   wrapThumb.slice(i, i+4).wrapAll("<div class=\"row anim-300 thum-row\"></div>");
                   $(".thum-row").each(function(y) {
                     $(this).addClass("item" + (y+1));
                     });
                 }
                 // ROW p3-content
                 var wrapContent = $(".p3-content");
                 for(var i = 0; i < wrapContent.length; i+=4) {
                   wrapContent.slice(i, i+4).wrapAll("<div class=\"row anim-300 p3-content-row\"></div>");
                   $(".p3-content-row").each(function(y) {
                     $(this).addClass("item" + (y+1));
                     });
                 }
                     if ($(window).width() < 768) {
                         var wrapThumb = $(".presta-3");
                         for(var i = 0; i < wrapThumb.length; i+=1) {
                           wrapThumb.slice(i, i+1).wrapAll("<div class=\"row anim-300 thum-row\"></div>");
                           $(".thum-row").each(function(y) {
                             $(this).addClass("item" + (y+1));
                             });
                         }
                         // ROW p3-content
                         var wrapContent = $(".p3-content");
                         for(var i = 0; i < wrapContent.length; i+=1) {
                           wrapContent.slice(i, i+1).wrapAll("<div class=\"row anim-300 p3-content-row\"></div>");
                           $(".p3-content-row").each(function(y) {
                             $(this).addClass("item" + (y+1));
                             });
                         }
                     }
                     else {
                        var wrapThumb = $(".presta-3");
                        for(var i = 0; i < wrapThumb.length; i+=4) {
                           wrapThumb.slice(i, i+4).wrapAll("<div class=\"row anim-300 thum-row\"></div>");
                           $(".thum-row").each(function(y) {
                             $(this).addClass("item" + (y+1));
                             });
                        }
                         // ROW p3-content
                         var wrapContent = $(".p3-content");
                         for(var i = 0; i < wrapContent.length; i+=4) {
                           wrapContent.slice(i, i+4).wrapAll("<div class=\"row anim-300 p3-content-row\"></div>");
                           $(".p3-content-row").each(function(y) {
                             $(this).addClass("item" + (y+1));
                             });
                         }
                     }
                 // ANIM AU CLICK
                 $(function() {
                     $('.p3-content').parent().css('height', 0);
                     $('.presta-3').click(
                         function() {
                             $(".p3-content").slideUp(300);
                             var idx_p3 = $('.presta-3').index(this);
                             $('.p3-content').eq(idx_p3).delay(300).slideDown(300);
                             var p3_content_height = $('.p3-content').eq(idx_p3).outerHeight();
                             $('.p3-content').parent().css('height', 0);
                             $('.p3-content').eq(idx_p3).parent().css('height', p3_content_height);
                     })
                 });
                 // CLICK CLOSE P3-CONTENT
                 $(function() {
                     $('.close-p3').click(
                         function() {
                         $('.p3-content-row').css('height', 0);
                         $('.p3-content').slideUp(300);
                     });
                 });

              // TEMPLATE SLIDER
            //  $('.template_slider').slick({
            //    infinite: true,
            //    slidesToShow: 1,
            //    slidesToScroll: 1,
            //    dots: true,
            //    autoplay: true,
            //    arrows: true
            //  });
             /* Prev / Next Links */

			// $('.template_slider2').slick({
            //    infinite: true,
            //    slidesToShow: 1,
            //    slidesToScroll: 1,
            //    dots: true,
            //    autoplay: true,
            //    arrows: true
            //  });

             var prevLink = $('h3.prev-link'),
                 nextLink = $('h3.next-link'),
                 prevW = prevLink.outerWidth(),
                 nextW = nextLink.outerWidth();
             $('h3.prev-link a .target').css('left', -prevW);
             $('h3.next-link a .target').css('right', -nextW);


             /* Modale */
             $( ".modale-parent" ).each(function() {
                 var last = $(this);
                 $(this).find('.modale-button').click(function() {
                     $('body').addClass("modale-open");
                     last.find('.modale').fadeIn("300");
                     last.find('.modale-content').addClass("animation-fade-up");
                     var coTop = last.find('.modale-content').outerHeight();
                     var coTmp = "calc(50% - " + coTop/2 + "px)";
                     last.find('.modale-content').css("top", coTmp);
                 });
                 $(this).find('.remove-modale').click(function() {
                     $('body').removeClass("modale-open");
                     $(this).parent().parent().parent().fadeOut("300");
                     $(this).parent().parent().parent().children('.modale-content').removeClass("animation-fade-up");
                 });
             });
             // /* Smooth Scroll */

             var $root = $("html, body");
             $("a").click(function() {
                 var href = $.attr(this, "href");
                 $root.animate({
                     scrollTop: $(href).offset().top
                 }, 500, function () {
                     window.location.hash = href;
                 });
                 return false;
             });

             outdatedBrowser({
                    lowerThan: 'transform',
                    languagePath: '../languages/outdatedbrowser/fr.html'
             })

    });

})(jQuery, this);

/* Page-loader */
setTimeout(function(){
	$( ".tl-storyslider" ).wrapAll("<div class='container'> </div>" );
	// var trait_height = jQuery(".tl-timemarker-timespan").height();
    // jQuery(".tl-timemarker-timespan").height(trait_height - 6)
}, 2000);




/*!--------------------------------------------------------------------
JAVASCRIPT "Outdated Browser"
Version:    1.1.2 - 2015
author:     Burocratik
website:    http://www.burocratik.com
* @preserve
-----------------------------------------------------------------------*/

var outdatedBrowser = function(options) {

    //Variable definition (before ajax)
    var outdated = document.getElementById("outdated");

    // Default settings
    this.defaultOpts = {
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: '../languages/outdatedbrowser/fr.html'
    }

    if (options) {
        //assign css3 property to IE browser version
        if (options.lowerThan == 'IE8' || options.lowerThan == 'borderSpacing') {
            options.lowerThan = 'borderSpacing';
        } else if (options.lowerThan == 'IE9' || options.lowerThan == 'boxShadow') {
            options.lowerThan = 'boxShadow';
        } else if (options.lowerThan == 'IE10' || options.lowerThan == 'transform' || options.lowerThan == '' || typeof options.lowerThan === "undefined") {
            options.lowerThan = 'transform';
        } else if (options.lowerThan == 'IE11' || options.lowerThan == 'borderImage') {
            options.lowerThan = 'borderImage';
        }
        //all properties
        this.defaultOpts.bgColor = options.bgColor;
        this.defaultOpts.color = options.color;
        this.defaultOpts.lowerThan = options.lowerThan;
        this.defaultOpts.languagePath = options.languagePath;

        bkgColor = this.defaultOpts.bgColor;
        txtColor = this.defaultOpts.color;
        cssProp = this.defaultOpts.lowerThan;
        languagePath = this.defaultOpts.languagePath;
    } else {
        bkgColor = this.defaultOpts.bgColor;
        txtColor = this.defaultOpts.color;
        cssProp = this.defaultOpts.lowerThan;
        languagePath = this.defaultOpts.languagePath;
    } //end if options


    //Define opacity and fadeIn/fadeOut functions
    var done = true;

    function function_opacity(opacity_value) {
        outdated.style.opacity = opacity_value / 100;
        outdated.style.filter = 'alpha(opacity=' + opacity_value + ')';
    }

    // function function_fade_out(opacity_value) {
    //     function_opacity(opacity_value);
    //     if (opacity_value == 1) {
    //         outdated.style.display = 'none';
    //         done = true;
    //     }
    // }

    function function_fade_in(opacity_value) {
        function_opacity(opacity_value);
        if (opacity_value == 1) {
            outdated.style.display = 'block';
        }
        if (opacity_value == 100) {
            done = true;
        }
    }

    //check if element has a particular class
    // function hasClass(element, cls) {
    //     return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
    // }

    var supports = ( function() {
        var div = document.createElement('div');
        var vendors = 'Khtml Ms O Moz Webkit'.split(' ');
        var len = vendors.length;

        return function(prop) {
            if (prop in div.style) return true;

            prop = prop.replace(/^[a-z]/, function(val) {
                return val.toUpperCase();
            });

            while (len--) {
                if (vendors[len] + prop in div.style) {
                    return true;
                }
            }
            return false;
        };
    } )();

    //if browser does not supports css3 property (transform=default), if does > exit all this
    if (!supports('' + cssProp + '')) {
        if (done && outdated.style.opacity !== '1') {
            done = false;
            for (var i = 1; i <= 100; i++) {
                setTimeout(( function(x) {
                    return function() {
                        function_fade_in(x);
                    };
                } )(i), i * 8);
            }
        }
    } else {
        return;
    } //end if

    //Check AJAX Options: if languagePath == '' > use no Ajax way, html is needed inside <div id="outdated">
    if (languagePath === ' ' || languagePath.length == 0) {
        startStylesAndEvents();
    } else {
        grabFile(languagePath);
    }

    //events and colors
    function startStylesAndEvents() {
        var btnClose = document.getElementById("btnCloseUpdateBrowser");
        var btnUpdate = document.getElementById("btnUpdateBrowser");

        //check settings attributes
        outdated.style.backgroundColor = bkgColor;
        //way too hard to put !important on IE6
        outdated.style.color = txtColor;
        outdated.children[0].style.color = txtColor;
        outdated.children[1].style.color = txtColor;

        //check settings attributes
        btnUpdate.style.color = txtColor;
        // btnUpdate.style.borderColor = txtColor;
        if (btnUpdate.style.borderColor) {
            btnUpdate.style.borderColor = txtColor;
        }
        btnClose.style.color = txtColor;

        //close button
        btnClose.onmousedown = function() {
            outdated.style.display = 'none';
            return false;
        };

        //Override the update button color to match the background color
        btnUpdate.onmouseover = function() {
            this.style.color = bkgColor;
            this.style.backgroundColor = txtColor;
        };
        btnUpdate.onmouseout = function() {
            this.style.color = txtColor;
            this.style.backgroundColor = bkgColor;
        };
    } //end styles and events


    // IF AJAX with request ERROR > insert french default
    var ajaxEnglishDefault = '<h6>Votre navigateur est obsolète!</h6>'
        + '<p>Mettez à jour votre navigateur pour afficher correctement ce site Web. <a href="http://outdatedbrowser.com/" ><div class="maj">Mettre à jour maintenant </div></a></p>'
        + '<p class="last"><a id="btnCloseUpdateBrowser"><div class="closediv">X</div></a></p>';

    //** AJAX FUNCTIONS - Bulletproof Ajax by Jeremy Keith **
    function getHTTPObject() {
        var xhr = false;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch ( e ) {
                try {
                    xhr = new ActiveXObject("Microsoft.XMLHTTP");
                } catch ( e ) {
                    xhr = false;
                }
            }
        }
        return xhr;
    }//end function

    function grabFile(file) {
        var request = getHTTPObject();
        if (request) {
            request.onreadystatechange = function() {
                displayResponse(request);
            };
            request.open("GET", file, true);
            request.send(null);
        }
        return false;
    } //end grabFile

    function displayResponse(request) {
        var insertContentHere = document.getElementById("outdated");
        if (request.readyState == 4) {
            if (request.status == 200 || request.status == 304) {
                insertContentHere.innerHTML = request.responseText;
            } else {
                insertContentHere.innerHTML = ajaxEnglishDefault;
            }
            startStylesAndEvents();
        }
        return false;
    }//end displayResponse

////////END of outdatedBrowser function
};


// BOUTON POUR REVENI EN HAUT DE LA PAGE
jQuery(document).scroll(function($) {
	var scrolltop = (jQuery(window).scrollTop());
	if (scrolltop > 300) {
		jQuery(".top-page").fadeIn();
	} else {
		jQuery(".top-page").fadeOut();
	}
});
