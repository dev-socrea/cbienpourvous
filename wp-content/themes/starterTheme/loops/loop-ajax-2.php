<!-- article -->
<article id="post-<?php the_ID(); ?>" <?php post_class("animation-fade-up"); ?>>

    <!-- post thumbnail -->
    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php if(has_post_thumbnail()){ // Check if thumbnail exists ?>
            <?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
        <?php }else{ ?>
            <div style="height: 120px; background-size: contain; background-repeat: no-repeat; background-image: url('<?=get_template_directory_uri().'/assets/img/gravatar.jpg'?>')"></div>
        <?php } ?>
    </a>
    <!-- /post thumbnail -->

    <!-- post title -->
    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
    <!-- /post title -->
    <?php html5wp_excerpt('html5wp_index'); ?>
</article>
<!-- /article -->