<?php /* Template Name: Timeline2 */ get_header();
?>

<div id="timeline-embed" style="width: 100%; height: 800px"></div>


<?php
	$context = stream_context_create(array('ssl'=>array(
		'verify_peer' => false,
		'verify_peer_name'=>false
	)));

	$fichierdebut = '{
		"title": {
			"media": {
				"url": "",
				"caption": "",
				"credit": ""
			},
			"text": {
				"headline": "Bonjour",
				"text": "Bonjour"
			}
		},
		"events": [';

			$compteur = 0;
			$args = array( 'post_type' => 'post', 'posts_per_page' => -1 );
			$loop = new WP_Query( $args );
			$recent_posts = wp_get_recent_posts( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
				$thumb_id = get_post_thumbnail_id();
				$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-tier', true);
				$exerpt_replace = str_replace("\"","'",get_the_excerpt());

				if ($compteur == 0) {
					$fichier[$compteur] = '{
			          "media": {
			            "url": "'.$thumb_url[0].'",
			            "caption": "",
			            "credit": ""
			          },
			          "start_date": {
			            "month": "'.get_the_date("n").'",
			            "day": "'.get_the_date("j").'",
			            "year": "'.get_the_date("Y").'"
			          },
			          "text": {
			            "headline": "'.get_the_title().'",
			            "text": "' . $exerpt_replace . '<a class=\'view-article\' href=\''.get_permalink().'\'>Voir l\'article</a>"
			          }
			        }
			        ';
				} else {
					$fichier[$compteur] = '
			            ,{
				          "media": {
				            "url": "'.$thumb_url[0].'",
				            "caption": "",
				            "credit": ""
				          },
				          "start_date": {
				            "month": "'.get_the_date("n").'",
				            "day": "'.get_the_date("j").'",
				            "year": "'.get_the_date("Y").'"
				          },
				          "text": {
				            "headline": "'.get_the_title().'",
				            "text": "' . $exerpt_replace . '<a class=\'view-article\' href=\''.get_permalink().'\'>Voir l\'article</a>"
				          }
				        }
			        ';
				}

			++$compteur;
	endwhile;

	for ($i=0; $i < count(getTweets()); $i++) {
		$date_tweet = getTweets()[$i]['created_at'];
		$text_text = getTweets()[$i]['text'];
		$text_text = str_replace("\n"," ",$text_text);
		$text_text = str_replace("\r"," ",$text_text);
		$text_text = str_replace("\t"," ",$text_text);
		if (isset(getTweets()[$i]['entities']['media'][0]['media_url'])) {
			$img_tweet = (getTweets()[$i]['entities']['media'][0]['media_url']);
		} else {
			$img_tweet = 'https://upload.wikimedia.org/wikipedia/fr/thumb/c/c8/Twitter_Bird.svg/1259px-Twitter_Bird.svg.png';
		}

		$fichier2[$i] = '
			,{
			  "media": {
				"url": "'.$img_tweet.'",
				"caption": "",
				"credit": ""
			  },
			  "start_date": {
				"month": "'.date("n", strtotime($date_tweet)).'",
				"day": "'.date("j", strtotime($date_tweet)).'",
				"year": "'.date("Y", strtotime($date_tweet)).'"
			  },
			  "text": {
				"headline": "Tweet",
				"text": "' . $text_text . '"
			  }
			}
		';
	}

	// VIDE ET REECRIS LE FICHIER POUR MAJ TOTALE
	unlink(get_template_directory() . "/boucle.json");
	$fileboucle = fopen(get_template_directory() . "/boucle.json", "a+", false, $context);
	for ($i=0; $i !=  $compteur; $i++) {
		fwrite($fileboucle,$fichier[$i]);
	}

	unlink(get_template_directory() . "/boucle2.json");
	$fileboucle2 = fopen(get_template_directory() . "/boucle2.json", "a+", false, $context);
	for ($i=0; $i < count(getTweets()); $i++) {
		fwrite($fileboucle2,$fichier2[$i]);
	}

	$fileboucleaff = file_get_contents(get_template_directory() . "/boucle.json");
	$fileboucleaff2 = file_get_contents(get_template_directory() . "/boucle2.json");


	$fichierfin = ']
	}';
	$fichierfinal = $fichierdebut."".$fileboucleaff."".$fileboucleaff2."".$fichierfin;

	$file = fopen(get_template_directory() . "/data.json", "w", false, $context);
	fwrite($file,$fichierfinal);
	fclose($file);
	fclose($fileboucle);
	fclose($fileboucle2);
?>


<script type="text/javascript">
// TIME LINE 2

$(document).ready(function() {
	var additionalOptions = {
              start_at_end: true,
              default_bg_color: {r:999, g:999, b:999},
              timenav_height: 250,
			  debug: true
            }

	jQuery.browser = {};
	(function () {
		jQuery.browser.msie = false;
		jQuery.browser.version = 0;
		if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
			jQuery.browser.msie = true;
			jQuery.browser.version = RegExp.$1;
		}
	})();


	timeline = new TL.Timeline('timeline-embed', '/wp-content/themes/starterTheme/data.json', additionalOptions);

});

</script>
<!-- /container-fluid -->
<?php get_footer(); ?>
