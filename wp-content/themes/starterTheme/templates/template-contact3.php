<?php /* Template Name: Contact3 */ get_header(); ?>
<?php while ( have_posts() ) : the_post();?>

	<main role="main" class="main-content">
		<div class="container-fluid tmplt-contact" style="background: background: -moz-linear-gradient(top, rgba(0,0,0,0.6) 0%, rgba(0,0,0,0.6) 1%, rgba(0,0,0,0.6) 100%);
		background: -webkit-linear-gradient(top, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 1%,rgba(0,0,0,0.6) 100%);
		background: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 1%,rgba(0,0,0,0.6) 100%), 
		url('<?php the_field('contact_bkg')?>');">
			<div class="container">
				<div class="row" >
					<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 form-contact3 p-l-0 p-r-0 m-b-50">
						<div class="contact-title p-t-50 p-b-50 m-b-30">
							<h1 class="open text-center m-t-0 fs-44"><?php the_title(); ?></h1>
							<div class="contact-content open fs-18 text-center"><?php the_content(); ?></div>
						</div>
						<?php echo (do_shortcode('[contact-form-7 id="4" title="Contact form 1"]')); ?>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php endwhile;?>
<?php get_footer(); ?>