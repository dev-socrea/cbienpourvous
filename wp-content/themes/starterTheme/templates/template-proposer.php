<?php /* Template Name: Proposer un bien */ get_header(); ?>
<main role="main" class="main-content">
	<?php $image_url = get_field('image_slider'); ?>
	<div class="container-fluid p-l-0 p-r-0 slide-proposer" style="background: background: -moz-linear-gradient(top, rgba(0,0,0,0.6) 0%, rgba(0,0,0,0.6) 1%, rgba(0,0,0,0.6) 100%);
	background: -webkit-linear-gradient(top, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 1%,rgba(0,0,0,0.6) 100%);
	background: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 1%,rgba(0,0,0,0.6) 100%), url(<?php echo $image_url ?>);">
		<h1 class="text-center fs-72 butler text-white m-t-150">Vous qui souhaitez louer ! </h1>
		<div class="container">
			<div class="col-lg-offset-2 col-lg-4 col-md-offset-2 col-md-4 col-sm-offset-2 col-sm-4 col-xs-offset-0 col-xs-12">
				<h2 class="open fs-24 text-white m-t-50">Pourquoi nous choisir ?</h2>
				<div class="open text-white fs-16 m-t-40">
					Cold calling can be a great way to generate quality leads. You get to speak to the gatekeepers and stakeholders,
				</div>
				<div class="open text-white fs-16 m-t-40">
					Posters had been a very beneficial marketing tool because it had paved to deliver an effective message that conveyed customer’s attention. It had been widely used for acquiring customer’s attention.
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 open">
				<h2 class="open fs-24 text-white m-t-50 titre-contact-proposer">Formulaire</h2>
				<div class="fs-16 text-white proposer-form">
					<?php echo (do_shortcode('[contact-form-7 id="194" title="Form proposer un bien"]')); ?>
				</div>
			</div>
		</div>
	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>
