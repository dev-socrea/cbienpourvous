<?php /* Template Name: Galerie */ get_header(); ?>

	<div class="container-fluid fil-dariane">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12 vertical-center">
					<h1>
						<?php echo "Galerie" ?>
					</h1>
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} ?>
				</div>
			</div>
		</div>
	</div>
<div class="container galerie_container">
	<?php $images = get_field('galerie'); if( $images ): ?> <!-- This is the gallery filed slug -->
		<?php foreach( $images as $image ): ?> <!-- This is your image loop -->
			<div class="">
				<a rel='first' id="single_image" href="<?php echo $image['url']; ?>">
					<img src="<?php echo $image['sizes']['thumbnail-500']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>
		<?php endforeach; ?> <!-- This is where the image loop ends -->
	<?php endif; ?> <!-- This is where the gallery loop ends -->
</div>

<?php foreach( $images as $image): ?>
	<script type="text/javascript">
	$(document).ready(function() {
		tuile_2p_1g();
		tuile_2p_1v();
		tuile_1g_1p_1v();
		tuile_1v_2p();
		tuile_1p_1v_1p();
		});
	</script>
<?php endforeach; ?>




<?php get_footer(); ?>
