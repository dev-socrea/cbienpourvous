<?php /* Template Name: Contact4 */ get_header(); ?>
<div class="container-fluid">
		<main role="main" class="main-content">
			<!-- section -->
			<section>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="flex row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mail-contact4">
							<div class="vertical-center">
								<h3 class="m-l-auto m-r-auto">Adresse / Téléphone</h3><br>
							</div>
						</div>
						<div class="col-lg-8 col-md-8  col-sm-8 col-xs-12 form-contact4">
							<h1 class="m-t-30 m-b-30"><?php the_title(); ?></h1>
							<?php the_content(); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
							<h2 class="text-center m-t-30 m-b-30">Parlez nous de votre projet</h2>
							<p class="text-justify m-b-30">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo expedita eaque ab fuga molestiae, inventore nisi at repellat accusamus natus, eveniet deleniti in placeat provident magni! Quam accusantium enim amet!</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 no-padding contact4">
							<div id="map"></div>
						</div>
					</div>

				</article>
				<!-- /article -->
			<?php endwhile; ?>

			<?php else: ?>
				<!-- article -->
				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
				</article>
				<!-- /article -->
			<?php endif; ?>
			</section>
			<!-- /section -->

			    <script async defer
			      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYM2FZ6PBFvla3XFMkE6xALHBw2KPY3LY&callback=initMap">
			    </script>
		</main>
</div>
<!-- /container-fluid -->
<?php get_footer(); ?>
