<?php /* Template Name: Contact5 */ get_header(); ?>
<div class="container-fluid">
		<main role="main" class="main-content">
			<!-- section -->
			<section>

				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="row" >
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact5-0">
							<h1 class="text-center m-t-30 m-b-30"><?php the_title(); ?></h1>
						</div>
					</div>
					<div class="flex row">
						<div class="col-lg-7 col-md-7 col-sm-7 hidden-xs contact5-1">
							<div class="table m-l-auto m-r-auto text-container-fluid">
								<span >Contact</span>
								<i class="fa fa-paper-plane-o p-l-30" aria-hidden="true"></i>
							</div>
						</div>
						<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 contact5-2">
							<span><b>+33</b> 07 00 00 00 00</span><br>
							<span><b>+33</b> 05 00 00 00 00</span>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact5-3 no-padding">
							<div id="map"></div>
							<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs p-30 flex contact5-4">
								<div class="m-l-auto m-r-20">
									<h3>Adresse</h3>
									<span>3616 Jones Street</span><br>
									<span>Fort Worth,</span><br>
									<span>TX 76107</span>
								</div>
								<div>
									<h3>Adresse 2</h3>
									<span>3616 Jones Street</span><br>
									<span>Fort Worth,</span><br>
									<span>TX 76107</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row contact5-5">
						<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12 p-t-30 form-contact5">
							<h3 class="text-center p-b-20">Parlez nous de votre projet</h3>
							<?php the_content(); ?>


						</div>
					</div>


				</article>
				<!-- /article -->
			<?php endwhile; ?>

			<?php else: ?>
				<!-- article -->
				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
				</article>
				<!-- /article -->
			<?php endif; ?>
			</section>
			<!-- /section -->

			    <script async defer
			      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYM2FZ6PBFvla3XFMkE6xALHBw2KPY3LY&callback=initMap">
			    </script>
		</main>
</div>
<!-- /container-fluid -->
<?php get_footer(); ?>
