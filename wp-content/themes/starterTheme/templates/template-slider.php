<?php /* Template Name: Slider */ get_header(); ?>


<main role="main" class="main-content">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<div class="container-fluid p-0">
		<div class="presta-title">
			<h1><?php the_title() ?></h1>
			<?php the_content('')?>
		</div>
		<h2>Images</h2>
		<div class="template_slider">
			<?php
			if( have_rows('temp_slider') ):
			while ( have_rows('temp_slider') ) : the_row();?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-image: url('<?php the_sub_field('image'); ?>');background-size: cover;background-position: center; height: 450px;">
				</div>
			<?php endwhile;
			else : endif; ?>
		</div>

<!-- *****************************************-->

		<h2>Derniers articles</h2>
		<?php
			$args = array('numberposts' => '3');
			$loop = new WP_Query( $args );
			$recent_posts = wp_get_recent_posts( $args );
		?>

		<div class="template_slider2">
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php
				$thumb_id = get_post_thumbnail_id();
				$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
				?>
				<?php echo '<a href="' . get_permalink() . '">' ?>
				<article class="m-t-20 flex col-lg-12 col-md-12 col-xs-12 recent-post-nav no-padding mosaique" style="height:450px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(<?php echo $thumb_url[0];?>);">
					<div class="slider-title-content">
						<div class="m-l-auto">
								<div class="m-l-10 slider-title">
									<?php the_title(); ?>
								</div>
							<?php
								$id = get_the_ID();
								$category = get_the_category($id); //
								$date = get_the_date('Y-m-d', $id);
							?>
							<div class="cat-mosaique m-l-10"><i class="fa fa-tag m-r-5" aria-hidden="true"></i> <?php echo $category[0]->name; ?></div>
							<div class="cat-mosaique m-l-10"><i class="fa fa-calendar-o m-r-5"></i><?php  echo $date; ?></div>
						<div>
					</div>
				</article>
			</a>
			<?php endwhile;?>
		</div>

<!--  **************************************  -->



		<h2>Derniers articles custom post type</h2>

		<?php

		$args = array( 'post_type' => 'first-custom-post', 'posts_per_page' => 3 );
		$loop = new WP_Query( $args );
		$recent_posts = wp_get_recent_posts( $args );
		?>
		<div class="template_slider">
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
			<?php
			$thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
			?>
			<?php echo '<a href="' . get_permalink() . '">' ?>
			<article class="m-t-20 flex col-lg-12 col-md-12 col-xs-12 recent-post-nav no-padding mosaique" style="height:450px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(<?php echo $thumb_url[0]?>);" >
				<div class="slider-title-content">
					<div class="m-l-auto">
						<div class="m-l-10 slider-title">
							<?php the_title(); ?>
						</div>
						<div class="cat-mosaique m-l-10"><i class="fa fa-tag m-r-5" aria-hidden="true"></i>
						  <?php
							  $term_list = wp_get_post_terms($post->ID, 'custom-taxonomy-1', array("fields" => "all"));
							  foreach($term_list as $term_single) {?>
							  	<?php echo $term_single->slug; ?>
						 	<?php
						 	}
						 	?>
						</div>
					</div>
				</div>
			</article>
		</a>
		<?php endwhile;?>
		</div>

		<?php endwhile; ?>
		<?php else: ?>
			<!--  -->
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
			</article>
		<?php endif; ?>

	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>
<!-- <div class="cat-mosaique m-l-10"><i class="fa fa-tag m-r-5" aria-hidden="true"></i>' . get_the_category( $recent["ID"] )[0]->name . '</div> -->
