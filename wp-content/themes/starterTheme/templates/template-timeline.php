<?php /* Template Name: Timeline */ get_header(); ?>
<main role="main" class="main-content">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<div class="container p-0">
			<div class="row presta-title">
				<h1><?php the_title() ?></h1>
				<?php the_content('')?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php
	$args = array( 'post_type' => 'post', 'posts_per_page' => -1 );
	$loop = new WP_Query( $args );
	$recent_posts = wp_get_recent_posts( $args );
	?>
	<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		?>
		<div class="detailtime">
			<div class="descriptif-content">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; ?>
	<div class="slidetime anim-300">
		<div class="slidetime2 anim-300">
			<div class="presta-4 anim-300">
				<div class="no-lazy anim-300">
						<?php
						$initdatestart = new DateTime();
						$start = new DateTime('2017-01-01'); // DateTime::createFromFormat('d/m/Y', '15/06/2015');
						$end = new DateTime(); // DateTime::createFromFormat('d/m/Y', '21/06/2015');
							foreach (new DatePeriod($start, DateInterval::createFromDateString('1 day'), $end) as $dt){
								$args = array( 'post_type' => 'post', 'posts_per_page' => -1 );
								$loop = new WP_Query( $args );
								$recent_posts = wp_get_recent_posts( $args );
								?>
								<div class="jourtime">
									<?php while ( $loop->have_posts() ) : $loop->the_post();?>
										<?php
										$thumb_id = get_post_thumbnail_id();
										$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true); ?>
										<?php if (get_the_date() == ($dt->format('d/m/Y'))){ ?>
											<div class="eventjourtime anim-300 <?php echo 'J'.($dt->format('j')) ?> <?php echo 'S'.($dt->format('W')) ?> <?php echo 'M'.($dt->format('m')) ?> <?php echo 'A'.($dt->format('Y')) ?>">
												<div class="dateevent">
													<?php the_date(); ?>
												</div>
												<?php the_title(); ?>
											</div>
											<?php } else {?>
											<?php } ?>
										<?php endwhile; ?>
									</div>
									<?php
									if (get_the_date() != ($dt->format('d/m/Y'))) { ?>
										<div class="jourtimenone anim-300 <?php echo 'J'.($dt->format('j')) ?> <?php echo 'S'.($dt->format('W')) ?> <?php echo 'M'.($dt->format('m')) ?> <?php echo 'A'.($dt->format('Y')) ?>">
											X
										</div>
										<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="btnzoom dezoom" type="button" name="button"><i class="fa fa-search-minus" aria-hidden="true"></i></div>
					<div class="btnzoom zoom" type="button" name="button"><i class="fa fa-search-plus" aria-hidden="true"></i></div>
					<div class="btnzoom semaine" type="button" name="button">Semaine</div>
					<div class="btnzoom mois" type="button" name="button">Mois</div>
					<div class="btnzoom annees" type="button" name="button">Années</div>
				</div>
			</main>
			<!-- /container-fluid -->
			<?php get_footer(); ?>
