<?php /* Template Name: Footer3 */ get_header(); ?>
<main role="main" class="main-content">

				<div class="home_slider">
					<?php while ( have_rows('slider_home') ) : the_row();
						if( get_row_layout() == 'slide' ):
							$titre = get_sub_field('titre');
							$image_url = get_sub_field('image');
							$description = get_sub_field('description');
							$lien = get_sub_field('lien');
						endif;?>
						<article class="flex col-lg-12 col-md-12 col-xs-12 recent-post-nav no-padding mosaique" style="height:450px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(<?php echo $image_url ?>);">
							<div class="slider-title-content">
								<div class="m-l-auto container">
										<div style="text-align: right; margin-top: 135px;" class="m-l-10 slider-title slideLeft">
											<?php echo $titre; ?>
										</div>
									<div style= "text-align: right; padding-left: 40%;"><?php echo $description; ?></div>
									<div style= "text-align: right;" class="btn_slider">
										<a href="<?php echo $lien; ?>"><b>Voir Plus</b></a>
									</div>
								<div>
							</div>
						</article>
					<?php endwhile;?>
				</div>

</main>
<!-- /container-fluid -->
<?php get_footer('003'); ?>
