<?php /* Template Name: Galerie 2*/ get_header(); ?>

	<div class="container-fluid fil-dariane">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12 no-padding flex">
					<h1>
						<?php echo "Galerie" ?>
					</h1>
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container galerie_container">
			<?php $images = get_field('galerie2'); if( $images ): ?> <!-- This is the gallery filed slug -->
				<?php foreach( $images as $image ): ?> <!-- This is your image loop -->
						<div class="">
							<a rel='first' id="single_image" href="<?php echo $image['url']; ?>">
								<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>"/>
							</a>
						</div>
				<?php endforeach; ?>
			<?php endif; ?>
	</div>


<?php foreach( $images as $image): ?>
	<script type="text/javascript">
	$(document).ready(function() {
		col4_col8();
		col8_col4();
		col4_col4_col_4();

		});

	</script>
<?php endforeach; ?>



<?php get_footer(); ?>
