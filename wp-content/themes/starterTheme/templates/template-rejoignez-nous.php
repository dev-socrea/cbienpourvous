<?php /* Template Name: Rejoignez-nous */ get_header(); ?>
<main role="main" class="main-content">
	<div class="container-fluid tmplt-rejoignez p-0" style="background: -moz-linear-gradient(47deg, rgba(255,255,255,0.58) 0%, rgba(171,171,171,0.00) 0%, rgba(47,47,47,0.73) 73%, rgba(47,47,47,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, #000000 100%);
	background: -webkit-linear-gradient(47deg, rgba(255,255,255,0.58) 0%, rgba(171,171,171,0.00) 0%, rgba(47,47,47,0.73) 73%, rgba(47,47,47,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, #000000 100%);
	background: linear-gradient(47deg, rgba(255,255,255,0.58) 0%, rgba(171,171,171,0.00) 0%, rgba(47,47,47,0.73) 73%, rgba(47,47,47,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, #000000 100%),
	url(<?php the_field('rejoignez_bkg'); ?>);">	
		<div class="row presta-title m-t-150 m-b-100 	">
			<h1 class="butler fs-64 text-white"><?php the_title('')?></h1>
			<?php
			if( have_rows('rejoignez_description') ):
				while ( have_rows('rejoignez_description') ) : the_row();?>
				<div class="open text-white fs-28 m-t-50"><?php the_sub_field('subtitle'); ?></div>
				<div class="open subtitle-excerpt text-white fs-16 m-t-15"><?php the_sub_field('text'); ?></div>
				<div class="open anim-300 subtitle-link text-white fs-16 m-t-30"><a class="anim-300 text-white fs-16" href="<?php the_sub_field('lien'); ?>">Devenez SHERPA, mais comment ?</a></div>
			<?php   endwhile;
			else :
			endif;
			?>
		</div>
	</div>
	<div class="container-fluid p-0">
		<div class="transition-orange"></div>
		<?php
		if( have_rows('prestations') ):
			while ( have_rows('prestations') ) : the_row();?>
		<div class="row presta-1 flex no-flex m-l-0 m-r-0">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-0 no-lazy presta-A" style="background: url('<?php the_sub_field('bloc_image'); ?>');">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-t-60 p-b-60 presta-B">
				<div class="text-container">
					<div class="fs-22 m-b-15 presta1-title open"><?php the_sub_field('titre_text'); ?></div>
					<div class="fs-18 m-t-15 presta1-subtitle m-b-15 open"><?php the_sub_field('sous_titre_text'); ?></div>
					<div class="fs-15 m-b-15 presta1-text open"><?php the_sub_field('description_text'); ?></div>
					<div class="anim-300 fs-16 presta1-link open"><a class="anim-300" href="<?php the_sub_field('lien_bouton'); ?>"><?php the_sub_field('texte_lien'); ?></a></div>
				</div>
			</div>
		</div>
			 <?php   endwhile;
		else :
		endif;
		?>
	</div>
	<div class="container-fluid rejoignez3 p-0" style="background: url('<?php the_field('rejoignez_avantages_bkg'); ?>');">
		<div class="row text-center m-l-0 m-r-0 m-b-80 m-t-50">
			<?php
			if( have_rows('rejoignez_avantages') ):
				while ( have_rows('rejoignez_avantages') ) : the_row();?>
				<div class="avantages-title butler text-white fs-32 m-b-30"><?php the_sub_field('title'); ?></div>
				<div class="avantages-text text-white fs-15 "><?php the_sub_field('text'); ?></div>
				<div class="anim-300 avantages-lien m-t-20"><a class="anim-300 text-white btn-orange" href="<?php the_sub_field('lien'); ?>">Ça m'intéresse</a></div>

			<?php   endwhile;
			else :
			endif;
			?>
		</div>
	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>
