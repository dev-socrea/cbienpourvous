<?php
/*
Template Name: Modale
*/
get_header();?>

<div class="container-fluid">
	<div class="row">
		<main role="main" class="col-lg-9 col-md-9 col-xs-9 main-content">
			<!-- section -->
			<section>
				<div class="modale-parent">
					<div class="modale-button">
						<a>Modale1</a>
					</div>
					<div class="modale">
					    <div class="modale-content anim-500">
					        <div class="col-lg-8 col-lg-offset-lg-2 col-md-8 col-md-offset-2 col-col-xs-8">
					            <h2>Je suis la modale 1</h2>
					        </div>
					        <div class="col-lg-2 col-md-2 col-col-xs-2">
					            <a class="remove-modale"><i class="fa fa-times" aria-hidden="true"></i></a>
					        </div>
					    </div>
					</div>
				</div>
				<div class="modale-parent">
					<div class="modale-button">
						<a>Modale2</a>
					</div>
					<div class="modale">
					    <div class="modale-content anim-500">
					        <div class="col-lg-8 col-lg-offset-lg-2 col-md-8 col-md-offset-2 col-col-xs-8">
					            <h2>Je suis la modale 2</h2>
					        </div>
					        <div class="col-lg-2 col-md-2 col-col-xs-2">
					            <a class="remove-modale"><i class="fa fa-times" aria-hidden="true"></i></a>
					        </div>
					    </div>
					</div>
				</div>
				<div class="modale-parent">
					<div class="modale-button">
						<a>Modale3</a>
					</div>
					<div class="modale">
					    <div class="modale-content anim-500">
					        <div class="col-lg-8 col-lg-offset-lg-2 col-md-8 col-md-offset-2 col-col-xs-8">
					            <h2>Je suis la modale 3</h2>
					        </div>
					        <div class="col-lg-2 col-md-2 col-col-xs-2">
					            <a class="remove-modale"><i class="fa fa-times" aria-hidden="true"></i></a>
					        </div>
					    </div>
					</div>
				</div>
				<?php get_template_part('loops/loop'); ?>
			</section>
			<!-- /section -->
		</main>
	</div>
</div>
<?php get_footer(); ?>
?>
