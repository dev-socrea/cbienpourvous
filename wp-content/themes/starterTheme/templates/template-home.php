<?php /* Template Name: Home */ get_header(); ?>
<main role="main home-tmplt" class="main-content">
	<div class="container-fluid home1 p-l-0 p-r-0">
		<div class="p-r-0 home_slider">
			<?php while ( have_rows('slider_home') ) : the_row();
				if( get_row_layout() == 'slide' ):
					$titre = get_sub_field('titre');
					$image_url = get_sub_field('image');
					$description = get_sub_field('description');
					$lien = get_sub_field('lien');
				endif;?>
				<div class="recent-post-nav no-padding mosaique" >
					<div class="flex no-flex">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 home-shadow" style="background-position: center; background-size: cover; background-repeat: no-repeat;
				 background: -moz-linear-gradient(-45deg, rgba(79,79,79,0.65) 0%, rgba(81,81,81,0.65) 1%, rgba(255,255,255,0) 100%); /* FF3.6-15 */
				 background: -webkit-linear-gradient(-45deg, rgba(79,79,79,0.65) 0%,rgba(81,81,81,0.65) 1%,rgba(255,255,255,0) 100%); /* Chrome10-25,Safari5.1-6 */
				 background: linear-gradient(135deg, rgba(79,79,79,0.65) 0%,rgba(81,81,81,0.65) 1%,rgba(255,255,255,0) 100%), /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				 url(<?php echo $image_url ?>);">
				 			<div class="home-shadow">
								<div class="text-container slider-title-content p-l-30">
									<div class="m-l-auto">
											<div style="text-align: left; margin-top: 135px;" class="m-l-10 slider-title slideLeft open">
												<h1 class="fs-48 open"><?php echo $titre; ?></h1>
											</div>
										<div style= "text-align: left;" class="m-l-10 open fs-20"><?php echo $description; ?></div>
										<div style= "text-align: left;" class="btn_slider m-l-10 m-t-30">
											<a class="text-white fs-20 open" href="<?php echo $lien; ?>">Cbienpourvous <b>C</b> quoi ?<span><i class="fa fa-angle-right fs-20" aria-hidden="true"></i></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="home-services col-lg-3 col-md-3 col-sm-3 col-xs-12 p-0">
							<a class="open fs-20 text-white text-center " href="#">
								<div class="home-service no-flex" style="background: -moz-linear-gradient(top, rgba(0,0,0,0.50) 0%, rgba(0,0,0,0.50) 1%, rgba(0,0,0,0.37) 100%); /* FF3.6-15 */
								background: -webkit-linear-gradient(top, rgba(0,0,0,0.50) 0%,rgba(0,0,0,0.50) 1%,rgba(0,0,0,0.37) 100%); /* Chrome10-25,Safari5.1-6 */
								background: linear-gradient(to bottom, rgba(0,0,0,0.50) 0%,rgba(0,0,0,0.50) 1%,rgba(0,0,0,0.37) 100%), /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
								url('<?php the_field('home_location');?>');">
									<div class="home-links">
										<p class="block m-l-auto m-r-auto fs-20">Location <br>de vacances</p>
										<p class="btn-orange open inline-block fs-16">Voir</p>
									</div>
								</div>
							</a>
							<a class="open fs-20 text-white text-center " href="#">
								<div class="home-service no-flex " style="background: -moz-linear-gradient(top, rgba(0,0,0,0.50) 0%, rgba(0,0,0,0.50) 1%, rgba(0,0,0,0.37) 100%); /* FF3.6-15 */
								background: -webkit-linear-gradient(top, rgba(0,0,0,0.50) 0%,rgba(0,0,0,0.50) 1%,rgba(0,0,0,0.37) 100%); /* Chrome10-25,Safari5.1-6 */
								background: linear-gradient(to bottom, rgba(0,0,0,0.50) 0%,rgba(0,0,0,0.50) 1%,rgba(0,0,0,0.37) 100%), /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
								 url('<?php the_field('home_conciergerie');?>');">
								 	<div class="home-links">
										<p class="m-l-auto m-r-auto fs-20">Conciergerie <br>immobilière</p>
										<p class="btn-orange open inline-block fs-16">Voir</p>
									</div>
								</div>
							</a>
							<a class="open fs-20 text-white text-center " href="#">
								<div class="home-service no-flex " style="background: -moz-linear-gradient(top, rgba(0,0,0,0.50) 0%, rgba(0,0,0,0.50) 1%, rgba(0,0,0,0.37) 100%); /* FF3.6-15 */
								background: -webkit-linear-gradient(top, rgba(0,0,0,0.50) 0%,rgba(0,0,0,0.50) 1%,rgba(0,0,0,0.37) 100%); /* Chrome10-25,Safari5.1-6 */
								background: linear-gradient(to bottom, rgba(0,0,0,0.50) 0%,rgba(0,0,0,0.50) 1%,rgba(0,0,0,0.37) 100%), /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
								 url('<?php the_field('home_rejoignez_nous');?>');">
								 	<div class="home-links">
										<p class="m-l-auto m-r-auto fs-20">Rejoignez-nous</p>
										<p class="btn-orange open inline-block fs-16">Voir</p>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			<?php endwhile;?>
		</div>
	</div>
	<div class="container-fluid home2 p-l-0 p-r-0">
		<div class="flex no-flex">
			<div class="home2-left col-lg-6 col-md-6 col-sm-6 col-xs-12" style="background: url('<?php the_field('home_conciergerie2');?>');"> 
			</div>
			<div class="home2-right col-lg-6 col-md-6 col-sm-6 col-xs-12 p-0">
				<div class="home2-oval">
					<div class="text-container p-l-50 p-b-80">
						<h3 class="p-t-80 m-b-30 m-t-0 open fs-24 text-white">Notre service de <br>conciergerie</h3>
						<div class="text-white m-b-30"><?php the_field('home2_text'); ?></div>
						<a class="home2-link fs-16 text-white" href="#">En savoir plus</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid home3">
		<div class="row">
			<p class=" p-30 text-center"><b>INTEGRATION DU FLUX ENOVA EN ATTENTE</b></p>
		</div>
	</div>
	<div class="container-fluid home4 p-l-0 	p-r-0">
		<div class=" flex no-flex">
			<div class="home4-left col-lg-7 col-md-7 col-sm-7 col-xs-12 p-0">
				<div class="home4-mask">
					<div class="text-container7-left p-b-50">
						<h3 class="p-t-50 m-b-15 m-t-0 open text-white fs-24">Devenir SHERPA, <span>mais comment?</span></h3>
						<div class="text-white m-b-30"><?php the_field('home4_left'); ?></div>
						<a class="home4-link fs-16 text-white" href="#">Le formulaire</a>
					</div>
				</div>
			</div>
			<div class="home4-right col-lg-5 col-md-5 col-sm-5 col-xs-12" style="background: url('<?php the_field('home4_right');?>');">
			</div>
		</div>
	</div>
	<div class="container-fluid home5" style="background: -moz-linear-gradient(top, rgba(0,0,0,0.75) 0%, rgba(0,0,0,0.75) 1%, rgba(0,0,0,0.37) 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top, rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 1%,rgba(0,0,0,0.75) 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom, rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 1%,rgba(0,0,0,0.75) 100%), /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			url('<?php the_field('home5_bkg');?>');">
		<div class="flex no-flex p-t-80 p-b-80">
			<?php $args = array( 
				'posts_per_page'   => 3,
				'offset'           => 0,
				'category'         => '', 
				'category_name'    => '', 
				'orderby'          => 'date',
				'order'            => 'DESC', 
				'include'          => '', 	'exclude'          => '',
				'meta_key'         => '', 
				'meta_value'       => '',
				'post_type'        => 'post', 	'post_mime_type'   => '', 
				'post_parent'      => '', 
				'author'	   	   => '', 
				'post_status'      => 'publish', 
				'suppress_filters' => true 
			); 
			 
			$myposts = get_posts( $args ); 
			 
			foreach ( $myposts as $post ) : setup_postdata( $post ); ?> 
			
			<a href="<?php the_permalink(); ?>">
				<div class="home5-actus col-lg-3 col-md-3 col-sm-3 col-xs-12" >
					<div style="background: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>'); background-size: cover;"></div>
					<h4 class="fs-16 text-white open"><?php the_title(); ?></h4>
					<a class="fs-14" href="<?php the_permalink(); ?>">En savoir +</a>
				</div>
			</a>

			<?php 
			endforeach; 
			wp_reset_postdata();
			?>

			<div class="col-lg-offset-1 col-lg-5 col-md-offset-1 col-md-5 col-sm-offset-1 col-sm-5 col-xs-12">
				<h3 class="open fs-24 text-white ">Nos Actualités</h3>
				<p class="m-b-30 open fs-16 text-white">Motivation How To Build Trust At Work</p>
				<a class="open text-white fs-14 btn-orange" href="#">Voir tout les articles</a>
			</div>
		</div>
	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>


