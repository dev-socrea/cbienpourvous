<?php /* Template Name: Conciergerie immobilière */ get_header(); ?>
<main role="main home-tmplt" class="main-content">
	<div class="container-fluid p-l-0 p-r-0">
		<?php $image_url = get_field('image_top'); ?>
		<div class="slide-conciergerie" style="background: background: -moz-linear-gradient(top, rgba(49,49,49,0.5) 0%, rgba(49,49,49,0.5) 1%, rgba(49,49,49,0.5) 100%);
		background: -webkit-linear-gradient(top, rgba(49,49,49,0.5) 0%,rgba(49,49,49,0.5) 1%,rgba(49,49,49,0.5) 100%);
		background: linear-gradient(to bottom, rgba(49,49,49,0.5) 0%,rgba(49,49,49,0.5) 1%,rgba(49,49,49,0.5) 100%), url(<?php echo $image_url ?>);">
			<h1 class="fs-72 titre-conciergerie butler text-center text-white"><?php the_title(); ?></h1>
			<h2 class="fs-28 text-center text-white open m-b-20 m-t-40"><?php the_field('sous_titre'); ?></h2>
			<div class="fs-16 description-slide-conciergerie text-white open"><?php the_field('description'); ?></div>
			<div class="container-lien-btn">
				<a href="#" class="btn-slide-conciergerie fs-16 open anim-300">Intendance, travaux & entretien</a>
				<a href="#" class="btn-slide-conciergerie fs-16 open anim-300">Check in / Check out</a>
				<a href="#" class="btn-slide-conciergerie fs-16 open anim-300 fix-translate">Nos services à la carte</a>
				<div class="click-select-conciergerie">
					<a href="#" class="btn-slide-conciergerie fs-16 open anim-300 click-select-conciergerie">Votre conciergerie <i class="fa fa-caret-down f-right rotate-click anim-300 m-r-10" aria-hidden="true"></i></a>
					<div class="select-items-conciergerie">
						<a href="#" class="select-item fs-16 open anim-300">
							Propriétaire
						</a>
						<a href="#" class="select-item fs-16 open anim-300">
							Locataire
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="transition-orange">
		</div>
		<?php $image_url = get_field('image_check'); ?>
		<div class="checkinout text-white" style="background: background: -moz-linear-gradient(top, rgba(0,0,0,0.6) 0%, rgba(0,0,0,0.6) 1%, rgba(0,0,0,0.6) 100%);
		background: -webkit-linear-gradient(top, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 1%,rgba(0,0,0,0.6) 100%);
		background: linear-gradient(to bottom, rgba(0,0,0,0.6) 0%,rgba(0,0,0,0.6) 1%,rgba(0,0,0,0.6) 100%), url(<?php echo $image_url ?>);">
			<div class="container">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 butler fs-48 m-t-30 text-right">
					<?php the_field('title_check') ?>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 m-t-50 open fs-15">
					<?php the_field('description_check') ?>
					<a class="btn-check m-t-20 m-b-50 fs-15" href="#">Plus d’informations</a>
				</div>
			</div>
		</div>
		<div class="transition-white">
		</div>
		<?php
		if( have_rows('prestations') ):
			$compt = 0;
			while ( have_rows('prestations') ) : the_row();
				$compt++; ?>
				<?php if ($compt == 1): ?>
				<div class="flex no-flex change-align-pair">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-gradient no-padding">
						<div class="img-background-presta">
							<div class="text-container p-l-50 p-t-80 p-b-80">
								<h3 class="open fs-24 titre-text-presta"><?php the_sub_field('titre_text'); ?></h3>
								<div class="open fs-16 m-b-10"><?php the_sub_field('sous_titre_text'); ?></div>
								<div class="open fs-15"><?php the_sub_field('description_text'); ?></div>
								<a class="btn-presta-conciergerie m-t-20" href="<?php the_sub_field('lien_bouton'); ?>"><?php the_sub_field('texte_lien'); ?></a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs p-0 row-presta-img" style="background-image: url(<?php the_sub_field('bloc_image'); ?>)">
					</div>
				</div>
			 	<?php
				endif; ?>
				<?php if ($compt == 2): ?>
				<div class="flex no-flex change-align-pair">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-gradient no-padding">
						<div class="text-container-left p-r-50 p-t-80 p-b-80">
							<h3 class="open fs-24 titre-text-presta"><?php the_sub_field('titre_text'); ?></h3>
							<div class="open fs-16 m-b-10"><?php the_sub_field('sous_titre_text'); ?></div>
							<div class="open fs-15"><?php the_sub_field('description_text'); ?></div>
							<a class="btn-presta-conciergerie m-t-20" href="<?php the_sub_field('lien_bouton'); ?>"><?php the_sub_field('texte_lien'); ?></a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs p-0 row-presta-img" style="background-image: url(<?php the_sub_field('bloc_image'); ?>)">
					</div>
				</div>
			 	<?php
		 		endif;
		 	endwhile;
		endif;
		?>
		<div class="transition-orange-center">
		</div>
		<?php $image_url = get_field('image_packs'); ?>
		<div class="packs-conciergerie butler text-white p-b-50" style="background: background: -moz-linear-gradient(top, rgba(49,49,49,0.85) 0%, rgba(49,49,49,0.85) 1%, rgba(49,49,49,0.85) 100%);
		background: -webkit-linear-gradient(top, rgba(49,49,49,0.85) 0%,rgba(49,49,49,0.85) 1%,rgba(49,49,49,0.85) 100%);
		background: linear-gradient(to bottom, rgba(49,49,49,0.85) 0%,rgba(49,49,49,0.85) 1%,rgba(49,49,49,0.85) 100%), url(<?php echo $image_url ?>);">
			<h3 class="text-center fs-40 m-0 p-t-80">Nos packs conciergerie :</h3>
			<h4 class="text-center fs-32">Une offre clef en main</h4>
			<div class="container">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div class="title-after fs-20 open">
						Nos packs propriétaires
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding item-pack-container open fs-20">
						<div class="item-pack">
							<div class="titre-pack m-t-20 m-l-15">
								Pack 1
							</div>
							<div class="description-pack m-t-10 m-l-15 fs-15 p-r-15 m-b-20">
								<?php the_field('description_pack_1') ?>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding item-pack-container open fs-20">
						<div class="item-pack">
							<div class="titre-pack m-t-20 m-l-15">
								Pack 2
							</div>
							<div class="description-pack m-t-10 m-l-15 fs-15 p-r-15 m-b-20">
								<?php the_field('description_pack_2') ?>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding item-pack-container open fs-20">
						<div class="item-pack">
							<div class="titre-pack m-t-20 m-l-15">
								Pack 3
							</div>
							<div class="description-pack m-t-10 m-l-15 fs-15 p-r-15 m-b-20">
								<?php the_field('description_pack_3') ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 p-r-0 col-xs-12">
					<div class="title-after-white fs-20 open">
						Notre pack locataires
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding item-pack-container open fs-20">
						<div class="item-pack-white">
							<div class="titre-pack m-t-20 m-l-15">
								Pack 4
							</div>
							<div class="description-pack m-t-10 m-l-15 fs-15 p-r-15 m-b-20">
								<?php the_field('description_pack_4') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a class="open text-center btn-presta-conciergerie m-t-20 btn-packs" href="#">Je choisis</a>
		</div>
		<?php
		if( have_rows('prestations') ):
			$compt = 0;
			while ( have_rows('prestations') ) : the_row();
				$compt++; ?>
				<?php if ($compt == 3): ?>
				<div class="flex no-flex change-align-pair">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-gradient no-padding">
						<div class="text-container p-l-50 p-t-80 p-b-80">
							<h3 class="open fs-24 titre-text-presta"><?php the_sub_field('titre_text'); ?></h3>
							<div class="open fs-16 m-b-10"><?php the_sub_field('sous_titre_text'); ?></div>
							<div class="open fs-15"><?php the_sub_field('description_text'); ?></div>
							<a class="btn-presta-conciergerie m-t-20" href="<?php the_sub_field('lien_bouton'); ?>"><?php the_sub_field('texte_lien'); ?></a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs p-0 row-presta-img" style="background-image: url(<?php the_sub_field('bloc_image'); ?>)">
					</div>
				</div>
				<?php endif; ?>
			 <?php
		 	endwhile;
		endif;
		?>
	</div>
</main>
<!-- /container-fluid -->
<?php get_footer(); ?>
