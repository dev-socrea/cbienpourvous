<?php /* Template Name: Home Staging */ get_header(); ?>
<main role="main" class="main-content">
	<div class="container-fluid home-staging1 p-0" style="background: -moz-linear-gradient(47deg, rgba(255,255,255,0.58) 0%, rgba(171,171,171,0.00) 0%, rgba(47,47,47,0.73)rgba(47,47,47,0.73) 73%, rgba(47,47,47,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, #000000 100%);
		background: -webkit-linear-gradient(47deg, rgba(255,255,255,0.58) 0%, rgba(171,171,171,0.00) 0%, rgba(47,47,47,0.73) 73%, rgba(47,47,47,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, #000000 100%);
		background: linear-gradient(47deg, rgba(255,255,255,0.58) 0%, rgba(171,171,171,0.00) 0%, rgba(47,47,47,0.73) 73%, rgba(47,47,47,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, rgba(46,46,46,0.73) 73%, #000000 100%),
		url(<?php the_field('travaux_bkg'); ?>);">	
		<div class="row presta-title m-t-150 m-b-100 	">
			<h1 class="butler fs-64 text-white"><?php the_title('')?></h1>
			<?php
			if( have_rows('travaux_description') ):
				while ( have_rows('travaux_description') ) : the_row();?>
				<div class="open text-white fs-28 m-t-50"><?php the_sub_field('subtitle'); ?></div>
				<div class="open subtitle-excerpt text-white fs-16 m-t-15"><?php the_sub_field('text'); ?></div>
			<?php   endwhile;
			else :
			endif;
			?>
		</div>
	</div>
	<div class="container-fluid home-staging2">
		<div class="row home-staging2-row">
		<?php
		if( have_rows('travaux_sections') ):
			while ( have_rows('travaux_sections') ) : the_row();?>

			<div class="travaux-sections col-lg-6 col-md-6 col-sm-6 col-xs-12 p-b-80 p-l-0 p-r-0" style="background: rgb(76,76,76,0.8);
			background: -moz-linear-gradient(-45deg, rgba(76,76,76,0.8) 0%, rgba(89,89,89,0.8) 100%);
			background: -webkit-linear-gradient(-45deg, rgba(76,76,76,0.8) 0%,rgba(89,89,89,0.8) 100%); 
			background: linear-gradient(135deg, rgba(76,76,76,0.8) 0%,rgba(89,89,89,0.8) 100%),
			url(<?php the_sub_field('background'); ?>);">
				<div class="section-title-bkg">
					<div class="text-container">
						<div class="section-title text-white butler p-t-20 p-b-20 fs-22 p-l-50"><?php the_sub_field('title'); ?></div>
					</div>
				</div>
				<div class="text-container">
					<div class="section-subtitle text-white open fs-16 m-t-50 p-l-50"><?php the_sub_field('subtitle'); ?></div>
					<div class="section-text text-white open fs-14 p-l-50"><?php the_sub_field('text'); ?></div>
				</div>
			</div>
		<?php  endwhile;
		else :
		endif;
		?>
		</div>			
	</div>
	<div class="container-fluid home-staging3 p-0">
		<?php
		if( have_rows('travaux_content') ):
			while ( have_rows('travaux_content') ) : the_row();?>
		<div class="row presta-1 flex no-flex m-l-0 m-r-0">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-0 no-lazy presta-C" style="background: url('<?php the_sub_field('bloc_image'); ?>');">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-t-60 p-b-60 presta-D">
				<div class="text-container">
					<div class="text-white fs-22 m-b-15 presta1-title open"><?php the_sub_field('title'); ?></div>
					<div class="text-white fs-18 m-t-15 presta1-subtitle m-b-15 open"><?php the_sub_field('subtitle'); ?></div>
					<div class="text-white fs-15 m-b-15 presta1-text open"><?php the_sub_field('text'); ?></div>
					<div class="btn-orange text-white anim-300 fs-16 presta1-link open"><a class="text-white anim-300" href="<?php the_sub_field('lien'); ?>">Plus d'informations</a></div>
				</div>
			</div>
		</div>
			 <?php   endwhile;
		else :
		endif;
		?>
	</div>
	<div class="container-fluid home-staging4 p-0" style="background: rgb(76,76,76,0.85);
	background: -moz-linear-gradient(top, rgba(76,76,76,0.85) 0%, rgba(19,19,19,0.85) 100%); 
	background: -webkit-linear-gradient(top, rgba(76,76,76,0.85) 0%,rgba(19,19,19,0.85) 100%); 
	background: linear-gradient(to bottom, rgba(76,76,76,0.85) 0%,rgba(19,19,19,0.85) 100%), 
	url('<?php the_field('travaux_bottom_bkg'); ?>');">
		<div class="row text-center m-l-0 m-r-0 m-b-80 m-t-50">
			<?php
			if( have_rows('travaux_bottom') ):
				while ( have_rows('travaux_bottom') ) : the_row();?>
				<div class="travaux-bottom-title butler text-white fs-32 m-b-30"><?php the_sub_field('title'); ?></div>
				<div class="travaux-bottom-text text-white fs-15 "><?php the_sub_field('text'); ?></div>
				<div class="anim-300 travaux-bottom-lien m-t-20"><a class="anim-300 text-white" href="<?php the_sub_field('lien'); ?>">Nous contacter</a></div>

			<?php   endwhile;
			else :
			endif;
			?>
		</div>
	</div>
	



</main>
<?php get_footer(); ?>
