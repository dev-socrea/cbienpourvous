<?php /* Template Name: Prestation3 */ get_header(); ?>
<main role="main" class="main-content">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<div class="container-fluid">
		<div class="row presta-title">
			<h1><?php the_title('')?></h1>
			<?php the_content('')?>
		</div>

		<div class="container-fluid">
				<?php
				if( have_rows('prestations') ):
				while ( have_rows('prestations') ) : the_row();?>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 p-0 presta-3" style="background-image: url('<?php the_sub_field('image'); ?>');background-size: cover;background-position: center; height: 180px;">
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p3-content">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-50">
							<div class="no-lazy col-lg-12 col-md-12 col-sm-12 col-xs-12 p3-img">
								<img class="" src="<?php the_sub_field('image'); ?>" alt="">
							</div>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 p-50">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<?php the_sub_field('descriptif'); ?>
								<div class="close-p3 pull-right">
									<i class="fa fa-times" aria-hidden="true"></i>
								</div>
							</div>
						</div>

					</div>
				<?php endwhile;
				else : endif; ?>
			</div>

		<?php endwhile; ?>
		<?php else: ?>
			<!--  -->
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'starterTheme' ); ?></h2>
			</article>
		<?php endif; ?>


</main>
<!-- /container-fluid -->
<?php get_footer(); ?>
