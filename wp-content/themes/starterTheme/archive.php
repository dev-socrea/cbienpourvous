<?php get_header(); ?>

<div class="container-fluid titre-fil">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 no-padding flex">
				<h1>
					<?php _e( 'Archives', 'starterTheme' ); ?>
				</h1>
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>
			</div>
		</div>
	</div>
</div>
<div class="container">
		<main role="main">
			<div class="row">
			<section>
				<div class="col-lg-9 col-md-9 col-xs-12 no-padding news-container">
					<?php get_template_part('loops/loop'); ?>
				</div>
			</section>
			<!-- /section -->
		<div class="col-lg-3 col-md-3 hidden-xs pull-left right-side-bar p-t-50">
            <?php get_sidebar(); ?>
        </div>

	</div>
	<div class="row">
	<div class="col-lg-9 col-md-9 col-xs-9 pagi text-center">
		<?php get_template_part('pagination');?>
	</div>
</div>
</main>
</div>
<?php get_footer(); ?>
