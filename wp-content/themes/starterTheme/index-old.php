<?php get_header();?>

	<div class="container-fluid titre-fil">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-xs-12 no-padding flex">
					<h1>
						<?php _e('Latest Posts', 'starterTheme'); ?>
					</h1>
					<?php if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					} ?>
				</div>
			</div>
		</div>
	</div>
<div class="container">
	<?php
	$args = array( 'numberposts' => '20' );
	$recent_posts = wp_get_recent_posts( $args );
	$i = 1;
	foreach( $recent_posts as $recent ){
		$excerpt = $recent["post_content"];
		$excerpt = substr($excerpt, 0, 75);
		$excerpt = $excerpt.'... <a href="'.get_permalink($recent["ID"]).'">'. __('View Article', 'starterTheme') .'</a>';
		$background = wp_get_attachment_image_src( get_post_thumbnail_id( $recent["ID"] ), 'full' );
			echo '
				<article class="col-lg-4 col-md-4 col-xs-4 anim-300 recent-post-nav no-padding mosaique-random" style="height:380px; background-position: center; background-size: cover; background-repeat: no-repeat; background-image: url(' . $background[0] . ');">
					<div class="titre-content m-l-auto">
						<div class="m-l-auto">
							<div class="m-l-10 titre-mosaique">'
								. $recent["post_title"].'
							</div>
							<div class="cat-mosaique m-l-10">' . get_the_category( $recent["ID"] )[0]->name . '
							</div>
							<div class="m-l-10">'
								. $excerpt . '
							</div>
						</div>
					</div>
				</article>';
	}
	wp_reset_query();

?>
</div>
<script type="text/javascript">
var $c = 1;
var marginType = jQuery("article.mosaique-random:nth-child(1)").height();

function tuile_2p_1g() {
  for (var i = 1; i <= 3; i++) {
		jQuery("article.mosaique-random:nth-child(" + $c + ")").each(function() {
			console.log($(this));
			if (i == 1) {
				//...
			} else if (i == 2) {
				$(this).removeClass( "col-lg-4 col-md-4 col-xs-4" );
				$(this).addClass( "col-lg-8 col-md-8 col-xs-8" );
				$(this).css({ height: marginType*2 });
			} else if (i == 3) {
				$(this).css({ marginTop: -marginType });
			}
		})
		$c = $c + 1;
	}
}

function tuile_2p_1v() {
  for (var i = 1; i <= 2; i++) {
		jQuery("article.mosaique-random:nth-child(" + $c + ")").each(function() {
			console.log($(this));
			if (i == 1) {
				//...
			} else if (i == 2) {
        $(this).css({ marginRight: marginType });
			} else if (i == 3) {
				//...
			}
		})
		$c = $c + 1;
	}
}

function tuile_1g_1p_1v() {
  for (var i = 1; i <= 2; i++) {
		jQuery("article.mosaique-random:nth-child(" + $c + ")").each(function() {
			console.log($(this));
			if (i == 1) {
        $(this).removeClass( "col-lg-4 col-md-4 col-xs-4" );
				$(this).addClass( "col-lg-8 col-md-8 col-xs-8" );
				$(this).css({ height: marginType*2 });
			} else if (i == 2) {
        $(this).css({ marginBottom: marginType });
			} else if (i == 3) {
				//...
			}
		})
		$c = $c + 1;
	}
}

function tuile_1v_2p() {
  for (var i = 1; i <= 2; i++) {
		jQuery("article.mosaique-random:nth-child(" + $c + ")").each(function() {
			console.log($(this));
			if (i == 1) {
        $(this).css({ marginLeft: marginType });
			} else if (i == 2) {
        //...
			} else if (i == 3) {
				//...
			}
		})
		$c = $c + 1;
	}
}

function tuile_1p_1v_1p() {
  for (var i = 1; i <= 2; i++) {
		jQuery("article.mosaique-random:nth-child(" + $c + ")").each(function() {
			console.log($(this));
			if (i == 1) {
        //...
			} else if (i == 2) {
        $(this).css({ marginLeft: marginType });
			} else if (i == 3) {
				//...
			}
		})
		$c = $c + 1;
	}
}

tuile_2p_1g();
tuile_2p_1v();
tuile_1g_1p_1v();
tuile_1v_2p();
tuile_1p_1v_1p();


</script>
<?php get_footer(); ?>
