<div class="top-page vertical-center anim-300">
	<a href="#top"><i class="fa fa-angle-up"></i></a>
</div>
<!-- footer -->
<footer class="footer" role="contentinfo">

	<div class="clearfix"></div>
	<!-- footer -->
	<div class="container">
		 <div class="row">
			  <div class="col-lg-12 col-md-12 col-xs-12 black footer-content sidebar-widget footer3">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div style="width:100px; float:left;" class="footer3_logo">
                          <?php include 'includes/logo.php'; ?>
                      </div>
                      <?php wp_nav_menu( array( 'theme_location' => 'social-menu', 'menu_class' => 'menu social' ) ); ?>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 footer3_text">
 					<p style="float:right;" class="m-t-15">Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                  </div>
			  </div>
		 </div>
	 </div>
	<!-- /footer -->
	<div class="clearfix"></div>
	<!-- sub-footer -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 sub-footer sidebar-widget text-center footer3_sub">
				<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('Sub Footer')) ?>
			</div>
		</div>
	</div>
	<!-- /sub-footer -->
	<div class="clearfix"></div>
</footer>
<!-- /footer -->
<div class="clearfix"></div>
</div>
<!-- /wrapper -->

<?php wp_footer(); ?>

<!-- outdatedbrowser -->
<div id="outdated"></div>
<!-- fin outdatedbrowser -->



<!-- <div id="cookie">
<div id="msgCookie">
<p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies et disposez d'une navigation optimale.</p>
</div>
<div id="cookieOk" onclick="cookieOk()">
<button class="anim-300"><?php _e('I agree', 'starterTheme') ?></button>
</div>
</div> -->



<script type="text/javascript">
verifCookie();
</script>

<script type="text/javascript">
// MISE EN PLACE DE FANCYBOX => LIGHTBOX POUR LES GALERIES PHOTOS
jQuery(document).ready(function($) {
	$('.gallery-icon a').has('img').addClass('fancybox');
	$('.gallery-icon a').has('img').attr('rel', 'group');
	$('.gallery-icon a').click(function () {
		var desc = $(this).attr('title');
		$('.gallery-icon- a').has('img').attr('title', desc, 'rel', [gallery] );
	});
	// LIGHTBOX INIT
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
});
</script>

<script type="text/javascript">
// init lazy sur les images
jQuery("img").each(function() {
	jQuery(this).addClass('lazy');
	var url = jQuery(this).attr("src");
	jQuery(this).removeAttr("src");
	jQuery(this).removeAttr("srcset");
	jQuery(this).attr("data-original", url);
});

// ne pas appliquer sur no-lazy
jQuery(".no-lazy img").each(function() {
	console.log(jQuery(this).attr("data-original"));
	var url2 = jQuery(this).attr("data-original");
	jQuery(this).attr("src", url2);
	jQuery(this).removeClass("lazy");
});

jQuery(function(){
	jQuery("img.lazy").lazyload({
		effect : "fadeIn",
		threshold : 500 // PIXELS A PARTIR DESQUELS L'IMAGE SE CHARGE
	});
});
</script>

<!-- THEME 2 POUR ARTICLE -->
<script type="text/javascript">
function theme2article() {
	$( ".news-container>div:first-of-type" ).removeClass( "col-lg-4 col-md-6 col-xs-12" );
	$( ".news-container>div:first-of-type" ).addClass( "col-lg-12 col-md-12 col-xs-12" );
}
</script>
<!--FIN THEME 2 POUR ARTICLE -->

<!-- THEME 3 POUR ARTICLE -->
<script type="text/javascript">
function theme3article() {
	var divs = $(".news-container section > div");
	for(var i = 1; i < divs.length; i+=2) {
		divs.slice(i, i+2).wrapAll("<div class='row'></div>");
	}

	$(".news-container section >div:first-of-type").wrapAll("<div class='row'></div>");

	jQuery(".news-container section >.row:nth-child(2n+0)>div:nth-child(1)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-8 col-md-8 col-xs-8" );
	})

	jQuery(".news-container section >.row:nth-child(2n+0)>div:nth-child(2)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-4 col-md-4 col-xs-4" );
	})

	jQuery(".news-container section >.row:nth-child(2n+1)>div:nth-child(1)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-4 col-md-4 col-xs-4" );
	})

	jQuery(".news-container section >.row:nth-child(2n+1)>div:nth-child(2)").each(function() {
		$(this).removeClass( "col-lg-4 col-md-6 col-xs-12" );
		$(this).addClass( "col-lg-8 col-md-8 col-xs-8" );
	})

	$( ".news-container section  .row:nth-child(1)>div" ).removeClass( "col-lg-4 col-md-4 col-xs-4" );
	$( ".news-container section  .row:nth-child(1)>div" ).addClass( "col-lg-12 col-md-12 col-xs-12" );
	console.log($( ".news-container section  .row:nth-child(1)>div" ));
}
</script>
<!--FIN THEME 3 POUR ARTICLE -->

<script type="text/javascript">

// THEME A APPLIQUER
var theme_actif = 3;


if (theme_actif == 1) {
} else if (theme_actif == 2) {
	theme2article();
} else if (theme_actif == 3) {
	theme3article();
}
</script>

<script type="text/javascript">
$( document ).ready(function() {
	$("#outdated").click(function() {
    	console.log("toto");
    	$("#outdated").fadeOut(300);
    });
});
</script>

<script>
function showResult(str) {
	if (str.length==0) {
		document.getElementById("livesearch").innerHTML="";
		document.getElementById("livesearch").style.border="0px";
		return;
	}
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {  // code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (this.readyState==4 && this.status==200) {
			document.getElementById("livesearch").innerHTML=this.responseText;
			document.getElementById("livesearch").style.border="1px solid #A5ACB2";
		}
	}
	xmlhttp.open("GET","livesearch.php?q="+str,true);
	xmlhttp.send();
}
</script>

<!-- analytics -->
<script>
(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
	(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
	l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
})(window,document,'script','<?php echo get_bloginfo("template_directory"); ?>/assets/js/analytics.js','ga');
ga('create', 'UA-XXXXXXXX-1', 'auto');
ga('send', 'pageview');
</script>

</body>
</html>
